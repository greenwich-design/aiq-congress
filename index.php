<?php
// Get header
get_header();
the_post();

echo '<div class="container">';
the_content();
echo '</div>';

// Get footer
get_footer();
?>