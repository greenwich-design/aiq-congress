<?php

function bb_check_resolved($topic_id) {
    $resolved_reply = get_post_meta( $topic_id, 'resolved_reply', true );
    if ( $resolved_reply ) :
        echo '<p class="ui-pill ui-pill--green">Resolved</p>';
    else :
        echo '<p class="ui-pill">Unresolved</p>';
    endif;
}