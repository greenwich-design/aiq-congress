<?php
// Template Name: Login

// Get header
get_header();
the_post();
?>

<main class="min-h-[calc(100vh-var(--nav-height))] lg:min-h-[calc(100vh-var(--nav-height-lg))] pb-[var(--nav-height)] lg:pb-[var(--nav-height-lg)] flex items-center">
    <div class="container">
        <div class="lg:max-w-screen-2xl lg:mx-auto grid gap-6 md:grid-cols-2 md:gap-12 lg:gap-24">

            <div class="space-y-6 md:space-y-10">
                <div class="flex flex-col gap-[2px] md:gap-[6px]">
                    <img class="max-lg:w-[167px] h-auto" src="<?php echo THEMEURL;?>/assets/img/autoiq-logo.svg" width="535" height="137" alt="<?php bloginfo('name');?>"/>
                </div>
                <div class="wysiwyg">
                    <p><strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut placerat viverra enim sed ullamcorper. Suspendisse quis leo at ipsum sagittis scelerisque. Sed tincidunt orci eu eros sodales blandit. Nam suscipit porta fermentum.</strong></p>
                    <p>Suspendisse rutrum nisi quis aliquam tincidunt. Curabitur elit dolor, dictum sed nisl sit amet, sodales eleifend erat. Morbi eu fermentum felis.</p>
                </div>
            </div>

            <div>
                <div class="md:max-w-[555px] space-y-5 lg:space-y-10 lg:-mt-4">
                    <h1 class="head-28 lg:head-88 after:hex-clip after:ml-1 after:lg:ml-2 translate-y-0.5 after:lg:translate-y-1 after:w-[8px] after:h-[9px] after:lg:w-[18px] after:lg:h-[20px] after:bg-red-1 after:inline-block">Login</h1>
                    
                    <div class="footer flex gap-5 items-end">
                        <p>Don’t have account yet?</p>
                        <a class="text-link" href="<?php bloginfo('url');?>/register">Sign Up</a>
                    </div>

                    <div id="lostpassword" class="[&:target+form]:block [&:target~form#login]:hidden"></div>

                    <form id="lostpasswordform" class="hidden form-styled ajax-auth space-y-5 lg:space-y-10" action="login" method="post">
                        
                        <hr class="bg-red-to-blue"/>
                        <?php wp_nonce_field('ajax-retrieve-nonce', 'security'); ?>
                        <!--<input type="hidden" id="security" name="security" value="9f8a6552e6">-->
                        <div class="group"> 
                            <label class="label" for="username">Username/Email Address</label>
                            <div class="field-wrap">
                                <input id="username" type="text" class="required peer !border-0" name="username">
                                <span class="z-[-1] absolute -inset-0.5 bg-red-to-blue bg-[length:200%_200%] animate-[animatedgradient_2s_ease_alternate_infinite] opacity-0 peer-focus:opacity-100"></span>
                            </div>
                        </div>
                        
                        <p class="c-login__status"></p>

                        <div class="footer flex gap-5 justify-between items-center lg:!my-14">
                            <a class="text-link" href="#login">Login</a>
                            <button class="btn-clip shrink-0" type="submit">Get new password</button>
                        </div>
                        <hr class="bg-red-to-blue"/>
                    </form>

                    <form id="login" class="form-styled ajax-auth space-y-5 lg:space-y-10" action="login" method="post">
                        
                        <hr class="bg-red-to-blue"/>
                        <?php wp_nonce_field('ajax-login-nonce', 'security'); ?>
                        <!--<input type="hidden" id="security" name="security" value="9f8a6552e6">-->
                        <div class="group"> 
                            <label class="label" for="username">Email Address</label>
                            <div class="field-wrap">
                                <input id="username" type="text" class="required peer !border-0" name="username">
                                <span class="z-[-1] absolute -inset-0.5 bg-red-to-blue bg-[length:200%_200%] animate-[animatedgradient_2s_ease_alternate_infinite] opacity-0 peer-focus:opacity-100"></span>
                            </div>
                        </div>
                        <div class="group">
                            <label class="label" for="password">Password</label>
                            <div class="field-wrap">
                                <input id="password" type="password" class="required peer !border-0" name="password">
                                <span class="z-[-1] absolute -inset-0.5 bg-red-to-blue bg-[length:200%_200%] animate-[animatedgradient_2s_ease_alternate_infinite] opacity-0 peer-focus:opacity-100"></span>
                            </div>
                        </div>

                        <p class="c-login__status"></p>

                        <div class="footer flex gap-5 justify-between items-center lg:!my-14">
                            <a class="text-link" href="#lostpassword">Forgotten Password?</a>
                            <button class="btn-clip shrink-0" type="submit">Login</button>
                        </div>
                        <hr class="bg-red-to-blue"/>
                    </form>

                    
                </div>
            </div>
        </div>
    </div>
</main>
<?php
echo '</div>';

// Get footer
get_footer();
?>