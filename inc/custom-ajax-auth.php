<?php
function ajax_auth_init(){	
	//wp_register_style( 'ajax-auth-style', get_template_directory_uri() . '/css/ajax-auth-style.css' );
	//wp_enqueue_style('ajax-auth-style');
	
	wp_register_script('validate-script', get_template_directory_uri() . '/scripts/lib/jquery.validate.min.js', array('jquery'), THEME_VERSION); 
    wp_enqueue_script('validate-script');
 
    wp_register_script('ajax-auth-script', get_template_directory_uri() . '/scripts/site/ajax-auth-script.min.js', array('jquery'), THEME_VERSION ); 
    wp_enqueue_script('ajax-auth-script');
    
    if ( is_multisite() ) {
        wp_localize_script( 'ajax-auth-script', 'ajax_auth_object', array( 
            'ajaxurl' => admin_url( 'admin-ajax.php' ),
            'redirecturl' => home_url(),
            'loadingmessage' => __('Sending user info, please wait...')
        ));
    } else {
        wp_localize_script( 'ajax-auth-script', 'ajax_auth_object', array( 
            'ajaxurl' => admin_url( 'admin-ajax.php' ),
            'redirecturl' => home_url(),
            'loadingmessage' => __('Sending user info, please wait...')
        ));

    }
 
    // Enable the user with no privileges to run ajax_login() in AJAX
    add_action( 'wp_ajax_nopriv_ajaxlogin', 'ajax_login' );
    add_action( 'wp_ajax_ajaxlogin', 'ajax_login' );
	// Enable the user with no privileges to run ajax_register() in AJAX
	add_action( 'wp_ajax_nopriv_ajaxregister', 'ajax_register' );
	add_action( 'wp_ajax_ajaxregister', 'ajax_register' );

    // Enable the user with no privileges to run ajax_register() in AJAX
	add_action( 'wp_ajax_nopriv_ajaxretrieve_password', 'ajax_retrieve_password' );
	add_action( 'wp_ajax_ajaxretrieve_password', 'ajax_retrieve_password' );
}
 
// Execute the action only if the user isn't logged in
//if (!is_user_logged_in()) {
    add_action('init', 'ajax_auth_init');
//}


function ajax_retrieve_password() {

    // First check the nonce, if it fails the function will break
    check_ajax_referer( 'ajax-retrieve-nonce', 'security' );

    $response = retrieve_password($_POST['username']);
    

    if ( is_wp_error($response) ) :
        $response = $response->get_error_message();
    else :
        $response = 'Check your email for the confirmation link.';
    endif;

    echo json_encode(array('loggedin'=>false, 'message'=>__($response)));

    die();
}
  
function ajax_login(){
 
    // First check the nonce, if it fails the function will break
    check_ajax_referer( 'ajax-login-nonce', 'security' );
    /*
    if (!wp_verify_nonce($_POST['security'], 'ajax-login-nonce')) {
        die(__('Security Check failed', 'textdomain'));
    }
    if ( $_POST['security'] !== '9f8a6552e6' ) :
        die(__('Security Check failed', 'textdomain'));
    endif;*/

    // Nonce is checked, get the POST data and sign user on
  	// Call auth_user_login
	auth_user_login($_POST['username'], $_POST['password'], 'Login'); 
	
    die();
}
 
function ajax_register(){
 
    // First check the nonce, if it fails the function will break
    check_ajax_referer( 'ajax-register-nonce', 'security' );

    /*
    if ( $_POST['security'] !== '9f8a6552e6' ) :
        die(__('Security Check failed', 'textdomain'));
    endif;*/

    $new_pass = wp_generate_password();
		
    // Nonce is checked, get the POST data and sign user on
    $info = array();
  	$info['user_nicename'] = $info['nickname'] = $info['display_name'] = $info['first_name'] = $info['user_login'] = sanitize_user($_POST['username']) ;
    $info['user_pass'] = $new_pass;
	$info['user_email'] = sanitize_email( $_POST['email']);


	// Register the user
    $user_register = wp_insert_user( $info );

    
 	if ( is_wp_error($user_register) ){	
		$error  = $user_register->get_error_codes()	;
		
		if(in_array('empty_user_login', $error))
			echo json_encode(array('toapprove'=>false,'loggedin'=>false, 'message'=>__($user_register->get_error_message('empty_user_login'))));
		elseif(in_array('existing_user_login',$error))
			echo json_encode(array('toapprove'=>false,'loggedin'=>false, 'message'=>__('This username is already registered.')));
		elseif(in_array('existing_user_email',$error))
        echo json_encode(array('toapprove'=>false,'loggedin'=>false, 'message'=>__('This email address is already registered.')));
    } else {
        
        update_user_meta( $user_register, 'company', $_POST['company'], true );
        update_user_meta( $user_register, 'phone', $_POST['phone'], true );

        if ( function_exists('get_field') ) :
            $acf__user_registration = get_field('user_registration', 'option');
            if ( isset($acf__user_registration['registration_type'] ) && $acf__user_registration['registration_type'] == 'approve' ) :
                $thankyou_msg = 'Thanks for registering - we’ll be in touch';
                if ( $acf__user_registration['register_confirm_message'] ) {
                    $thankyou_msg = $acf__user_registration['register_confirm_message'];
                }

                reg_successful_toapprove($thankyou_msg);
            else :
                // instant login
                wp_new_user_notification($user_register, null, 'user');
	            auth_user_login($info['nickname'], $new_pass, 'Registration');
            endif;
        else :
            /**
             * If ACF not installed, go for instant login
             */

            // instant login
            wp_new_user_notification($user_register, null, 'user');
	        auth_user_login($info['nickname'], $new_pass, 'Registration');

        endif;
       
    }
 
    die();
}
 
function auth_user_login($user_login, $password, $login)
{
	$info = array();
    $info['user_login'] = $user_login;
    $info['user_password'] = $password;
    $info['remember'] = true;
	
	$user_signon = wp_signon( $info, '' ); // From false to '' since v4.9
    if ( is_wp_error($user_signon) ){
		echo json_encode(array('toapprove'=>false,'loggedin'=>false, 'message'=>__('Wrong username or password.')));
    } else {
		wp_set_current_user($user_signon->ID); 
        echo json_encode(array('toapprove'=>false,'loggedin'=>true, 'message'=>__($login.' successful, redirecting...')));
    }
	
	die();
}

function reg_successful_toapprove($thankyou_msg)
{
	echo json_encode(array('toapprove'=>true,'loggedin'=>false, 'message'=>__('Registration successful '),'thankyou'=>__($thankyou_msg)));
	
	die();
}