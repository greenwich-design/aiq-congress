<div data-bbmodal="steps" id="steps" class="fixed flex inset-0 bg-[#FAFBFD]/80 z-50 py-10 px-6 overflow-auto invisible opacity-0 transition-all">
	<div class="fixed inset-0 cursor-pointer" data-bbmodalc="steps"></div>
	<div class="m-auto max-w-[820px] w-full relative space-y-5">
		<div class="flex justify-end">
			<button class="shrink-0" data-bbmodalc="steps">
				<img src="<?php echo THEMEURL;?>/assets/img/bbpress/close.svg" alt="Close" width="20"/>
			</button>
		</div>
		<?php 
		$form_type = 'insert_topic';
		include(locate_template('template-parts/steps-form.php'));?>
	</div>
</div>