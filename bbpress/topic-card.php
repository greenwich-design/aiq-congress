<article id="bbp-topic-<?php bbp_topic_id(); ?>" <?php bbp_topic_class($post->ID, array('border border-[#E9EAED] bg-white shadow-[0_0_26px_0_#E8E9EE] rounded-[4.4px] select-none') ); ?>>
	<header class="flex px-6 py-3 border-b border-[#E9EAED] space-x-5 justify-between">

		<h3 class="u-h3">
			
			<?php do_action( 'bbp_theme_before_topic_title' ); ?>

			<a class="bbp-topic-permalink hover:underline" href="<?php bbp_topic_permalink(); ?>"><?php bbp_topic_title(); ?></a>

			<?php do_action( 'bbp_theme_after_topic_title' ); ?>
		</h3>

		<div>

			<div class="flex space-x-4 items-center">
				<?php 
				// show resolved status
				bb_check_resolved(bbp_get_topic_id());?>

				<?php
				$topic_author_id = bbp_get_topic_author_id(bbp_get_topic_id());
				$current_author_id = get_current_user_id();
				
				// check if current users post
				if ( $topic_author_id == $current_author_id || current_user_can( 'manage_options' ) ) :?>
				<a href="<?php the_permalink();?>?edit" class="items-center h-[23px] text-[#A1A6A9] hover:text-blue-1 mt-[2px] shrink-0 flex space-x-1 cursor-pointer" title="Edit">
					<span class="rounded-full w-[6px] h-[6px] bg-current"></span>
					<span class="rounded-full w-[6px] h-[6px] bg-current"></span>
					<span class="rounded-full w-[6px] h-[6px] bg-current"></span>
				</a>
				<?php endif;?>

			</div>
		</div>
	</header>


	
	<div class="flex space-x-4 px-6 py-5">
		<div class="!rounded-full overflow-hidden [&>*]:!m-0">
			<?php global $post; 
			$topic_author = bbp_get_topic_author_id(bbp_get_topic_id());
			echo bbp_get_topic_author_avatar( bbp_get_topic_id(), 44 ) ?>
		</div>


		<div>
			<?php 
			// Get some useful reply information
			$user_id    = get_post_field( 'post_author', bbp_get_topic_id() );
			$author_url = bbp_get_user_profile_url( $user_id );
			$anonymous  = bbp_is_reply_anonymous( bbp_get_topic_id() );
			$author = get_the_author_meta( 'display_name', $user_id );

			$title  = empty( $anonymous )
				? esc_attr__( "View %s's profile",  'bbpress' )
				: esc_attr__( "Visit %s's website", 'bbpress' );

			$link_title = sprintf( $title, $author );

			// Only wrap in link if profile exists
			if ( empty( $anonymous ) && bbp_user_has_profile( $user_id ) ) {
				$author_link = sprintf( '<a href="%1$s"%2$s%3$s>%4$s</a>', esc_url( $author_url ), $link_title, ' class="bbp-author-link"', $author );?>
				<h4 class="text-[15px] !my-0 font-semibold tracking-[-0.22px] text-blue-1 leading-[23px] capitalize"><?php echo $author_link;?></h4>
			<?php }?>
			
			<p class="font-semibold text-[12px]"><?php bbp_reply_post_date(0, true);//bbp_topic_freshness_link(); ?></p>
			
		</div>
	</div>

	<div class="px-4">
		<div class="bg-[#FBFBFC] border-[#E9EAED] border rounded-[4.4px]">
            <?php
            $vehicle_make = get_post_meta( get_the_ID(), 'vehicle_make', true );
            $vehicle_model = get_post_meta( get_the_ID(), 'vehicle_model', true );?>

            <?php if ( $vehicle_make || $vehicle_model ) :?>
			<div class="text-[15px] font-normal tracking-[-0.22px] text-blue-1 leading-[23px] px-4 py-2 border-b border-[#E9EAED]">
                <span class="make font-semibold"><?php echo $vehicle_make;?></span> <?php echo $vehicle_model;?>
			</div>
            <?php endif;?>

			<div class="comments px-4 py-3 pb-6 space-y-5">
				<?php
                $customer_comments = get_post_meta( get_the_ID(), 'customer_comments', true );
				remove_filter( 'the_content', 'wpautop' );
                if ( $customer_comments ) :
                    if ( strlen($customer_comments) > 200 ) :
                        $str = substr($customer_comments,0,200);
						$str = nl2br($str);
                        echo wpautop($str.'...');
                    else :
						$str = nl2br($customer_comments);
                        echo wpautop($customer_comments);
                    endif;
                    
                endif;?>
			</div>
		</div>
	</div>

	<div class="hidden text-[12px] text-[#A4A8AC] font-semibold px-4 py-5 flex gap-3 flex-wrap select-none items-center">
		<?php //echo bbp_get_topic_favorite_link();?>
		<div class="flex space-x-2 items-center shrink-0">
			<span><img src="<?php echo THEMEURL;?>/assets/img/bbpress/like.svg" alt="Like"/></span>
			<span>Like</span>
		</div>
		<div class="flex space-x-2 items-center shrink-0">
			<span><img src="<?php echo THEMEURL;?>/assets/img/bbpress/comment.svg" alt="Comment"/></span>
			<span>Comment</span>
		</div>
		<div class="flex space-x-2 items-center shrink-0">
			<span><img src="<?php echo THEMEURL;?>/assets/img/bbpress/share.svg" alt="Share"/></span>
			<span>Share</span>
		</div>
	</div>

	<div class="px-4 py-5 space-x-5">
		<a class="btn-primary" href="<?php the_permalink();?>">View Topic</a>
		<?php
		// check if current users post
		if ( $topic_author_id == $current_author_id || current_user_can( 'manage_options' ) ) :?>
		<a class="edit-btn" href="<?php the_permalink();?>?edit">Edit Topic</a>
		<?php endif;?>

	</div>
</article>