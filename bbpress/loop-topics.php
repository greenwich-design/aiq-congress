<?php

/**
 * Topics Loop
 *
 * @package bbPress
 * @subpackage Theme
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

do_action( 'bbp_template_before_topics_loop' ); ?>

<ul id="bbp-forum-<?php bbp_forum_id(); ?>" class="bbp-topics">
	

	<li class="bbp-body !space-y-5">

		<?php 
		if ( !bbp_topics() ) :?>
		<div class="space-y-10 mb-10">
			<h1 class="u-h1">Nothing found</h1>
			<a class="btn-primary my-10" href="<?php the_permalink();?>">Back to <?php the_title();?></a>
		</div>
		<?php endif;

		while ( bbp_topics() ) : bbp_the_topic(); ?>

			<?php bbp_get_template_part( 'loop', 'single-topic' ); ?>

		<?php endwhile; ?>

	</li>

	
</ul><!-- #bbp-forum-<?php bbp_forum_id(); ?> -->

<?php do_action( 'bbp_template_after_topics_loop' );
