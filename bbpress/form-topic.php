<?php

/**
 * New/Edit Topic
 *
 * @package bbPress
 * @subpackage Theme
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

if ( ! bbp_is_single_forum() ) : ?>

<div id="bbpress-forums" class="bbpress-wrapper">

	<?php if ( ! bbp_is_topic_edit() ) : ?>
	<?php bbp_breadcrumb(); ?>
	<?php endif;?>

<?php endif; ?>

<?php if ( bbp_is_topic_edit() ) : ?>

	<?php //bbp_topic_tag_list( bbp_get_topic_id() ); ?>

	<?php //bbp_single_topic_description( array( 'topic_id' => bbp_get_topic_id() ) ); ?>

	<?php //bbp_get_template_part( 'alert', 'topic-lock' ); ?>

	<h1 class="u-h1 mb-10">Editing: "<?php the_title();?>"</h1>

<?php endif; ?>

<?php if ( bbp_current_user_can_access_create_topic_form() ) : ?>

	<div class="max-w-[820px] mx-auto">
	<?php 
	$form_type = 'edit_topic';
	include(locate_template('template-parts/steps-form.php'));?>
	</div>

<?php elseif ( bbp_is_forum_closed() ) : ?>

	<div id="forum-closed-<?php bbp_forum_id(); ?>" class="bbp-forum-closed">
		<div class="bbp-template-notice">
			<ul>
				<li><?php printf( esc_html__( 'The forum &#8216;%s&#8217; is closed to new topics and replies.', 'bbpress' ), bbp_get_forum_title() ); ?></li>
			</ul>
		</div>
	</div>

<?php else : ?>

	<div id="no-topic-<?php bbp_forum_id(); ?>" class="bbp-no-topic">
		<div class="bbp-template-notice">
			<ul>
				<li><?php is_user_logged_in()
					? esc_html_e( 'You cannot create new topics.',               'bbpress' )
					: esc_html_e( 'You must be logged in to create new topics.', 'bbpress' );
				?></li>
			</ul>
		</div>

		<?php if ( ! is_user_logged_in() ) : ?>

			<?php bbp_get_template_part( 'form', 'user-login' ); ?>

		<?php endif; ?>

	</div>

<?php endif; ?>

<?php if ( ! bbp_is_single_forum() ) : ?>

</div>

<?php endif;
