<?php

/**
 * Replies Loop - Single Reply
 *
 * @package bbPress
 * @subpackage Theme
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

$rep_id = bbp_get_reply_id();

if ( isset($res_id) && !empty($res_id) ) {
	$rep_id = $res_id;
}
?>
<div id="post-<?php echo $rep_id; ?>-resolved" class="p-[3px]">

    <div class=" bg-white aside-clip-inner p-5 py-8 lg:p-11 space-y-5 lg:space-y-10">
        <header class="flex space-x-5 items-center font-medium">
            <?php 

            // Get some useful reply information
            $user_id    = get_post_field( 'post_author', $rep_id );
            $author_url = bbp_get_user_profile_url( $user_id );
            $anonymous  = bbp_is_reply_anonymous( bbp_get_topic_last_active_id() );
            $author = get_the_author_meta( 'display_name', $user_id );
            $first_name = get_the_author_meta( 'first_name', $user_id );
            $last_name = get_the_author_meta( 'last_name', $user_id );

            if ( $first_name && $last_name ) {
                $author = $first_name . ' ' . $last_name;
            }

            $title  = empty( $anonymous )
                ? esc_attr__( "View %s's profile",  'bbpress' )
                : esc_attr__( "Visit %s's website", 'bbpress' );

            $link_title = sprintf( $title, $author );?>

            <div class="flex shrink-0 text-[16px] lg:text-[18px] font-bold gap-3 items-center ">
                <button data-marksolved class="btn-clip btn-clip--green-outline transition-all duration-200 ease-in-out"><span class="bg-white text-green-1 transition-all duration-200 ease-in-out">Resolution</span></button>
                <div class="flex gap-2 items-center">
                <?php
                // Only wrap in link if profile exists
                if ( empty( $anonymous ) && bbp_user_has_profile( $user_id ) ) {
                    $author_link = sprintf( '<a href="%1$s" >%4$s</a>', esc_url( $author_url ), $link_title, ' class="bbp-author-link"', $author );?>
                    <?php echo $author_link;?>
                <?php }?>
                </div>
            </div>
            
            <div class="text-[12px] lg:text-[16px]">
                <p class="lg:pt-0.5">
                    <?php 
                    $now_date = get_the_date('j F Y',$rep_id);
                    $now_time = get_the_date('g:i:s A',$rep_id);
                    //echo bbp_get_topic_post_date(bbp_get_topic_id(), false, true);//bbp_topic_freshness_link(); ?>
                    <?php echo $now_date;?> <span class="">&nbsp;/&nbsp;</span> <?php echo $now_time;?>
                </p>
            </div>
        </header>

        <div class="">
                    
            <div class="space-y-5 lg:space-y-10">
                <?php 
                $reply_to = bbp_get_form_reply_to();

                if ( $reply_to ) :
                    $post = $reply_to;
                    setup_postdata( $post );

                    $uid = bbp_get_topic_author_id($reply_to);

                    $author = get_the_author_meta( 'display_name', $uid );
                    $first_name = get_the_author_meta( 'first_name', $uid );
                    $last_name = get_the_author_meta( 'last_name', $uid );

                    if ( $first_name && $last_name ) {
                        $author = $first_name . ' ' . $last_name;
                    }
                    ?>

                    <div class="grid gap-5 md:grid-cols-[auto_1fr] pt-5">
                        <div class="flex text-[14px] lg:text-[16px] whitespace-nowrap gap-2"><span>Reply to</span>
                            <strong class="text-[16px] lg:text-[18px]"><?php echo $author;?></strong>
                        </div>

                        <div class="wysiwyg bg-white p-5 lg:p-7">
                            <?php echo get_the_content(); ?>
                        </div>
                    </div>
                <?php endif; wp_reset_postdata();
                ?>

                <div class="comment">

                    <?php do_action( 'bbp_theme_before_reply_content' ); ?>


                    <div class="wysiwyg [&>.bbp-reply-revision-log]:hidden">
                        <?php echo bbp_get_reply_content($rep_id); ?>
                    </div>

                    <?php
                    $current_author_id = get_current_user_id();?>

                    <?php 
                    if ( (bbp_get_reply_author_id() == $current_author_id || current_user_can( 'manage_options' ) ) ) :?>
                        <div class="[&>*]:text-link mt-5">
                        <?php echo bbp_get_reply_edit_link(array( "id" => $rep_id));?>
                        </div>
                    <?php endif;?>

                    <?php do_action( 'bbp_theme_after_reply_content' ); ?>

                </div>

            </div>
            

            <div>

                <div class="flex justify-end gap-5">
                    <?php
                    $resolved_reply = get_post_meta( bbp_get_topic_id(), 'resolved_reply', false );
                    $topic_author_id = bbp_get_topic_author_id(bbp_get_topic_id());
                    $current_author_id = get_current_user_id();?>

                    <?php 
                        if ( ($topic_author_id == $current_author_id || current_user_can( 'manage_options' ) ) && in_array($rep_id, $resolved_reply) ) :?>
                            <div class="">
                                <button data-marksolved="false" data-topicid="<?php echo bbp_get_topic_id();?>" data-replyid="<?php echo $rep_id; ?>" class="btn-clip">Unmark as Resolution</button>
                            </div>
                            <?php elseif ( ($topic_author_id == $current_author_id || current_user_can( 'manage_options' ) )  ) :?>
                            <div class="">
                                <button data-marksolved data-topicid="<?php echo bbp_get_topic_id();?>" data-replyid="<?php echo $rep_id; ?>" class="btn-clip btn-clip--green-outline transition-all duration-200 ease-in-out"><span class="bg-grey-1 text-green-1 transition-all duration-200 ease-in-out">Mark as Resolution</span></button>
                            </div>
                        <?php endif;
                    ?>

                    <div class="[&>*]:btn-clip">
                    <?php echo bbp_get_reply_to_link();?>
                    </div>
                </div>
            </div>

            <?php if ( current_user_can( 'manage_options' ) ) :?>
            <div class="[&>*]:!float-none">
                <?php bbp_reply_admin_links(); ?>
            </div>
            <?php endif;?>
        </div>
    </div>

</div><!-- #post-<?php echo $rep_id; ?> -->


