<?php
$topic_author_id = bbp_get_topic_author_id(bbp_get_topic_id());
$current_author_id = get_current_user_id();?>

<div class="">

	<article id="bbp-topic-<?php bbp_topic_id(); ?>" <?php bbp_topic_class($post->ID, array() ); ?>>
		
		<div class="grid gap-7 md:gap-10 lg:gap-x-14 md:grid-cols-[auto_minmax(auto,min-content)] lg:grid-cols-[auto_minmax(396px,min-content)]">

			<header class="space-y-1 lg:space-y-9">

				<h1 class="lg:min-h-[130px]">
					
					<?php do_action( 'bbp_theme_before_topic_title' ); ?>

					<span class="head-22 lg:head-66"><?php bbp_topic_title(); ?></span>

					<?php do_action( 'bbp_theme_after_topic_title' ); ?>
				</h1>

				<div class="flex space-x-7 items-center">
					<?php 
					// Get some useful reply information
					$user_id    = get_post_field( 'post_author', bbp_get_topic_id() );
					$author_url = bbp_get_user_profile_url( $user_id );
					$anonymous  = bbp_is_reply_anonymous( bbp_get_topic_id() );
					$author = get_the_author_meta( 'display_name', $user_id );
					$first_name = get_the_author_meta( 'first_name', $user_id );
					$last_name = get_the_author_meta( 'last_name', $user_id );

					if ( $first_name && $last_name ) {
						$author = $first_name . ' ' . $last_name;
					}

					$title  = empty( $anonymous )
						? esc_attr__( "View %s's profile",  'bbpress' )
						: esc_attr__( "Visit %s's website", 'bbpress' );

					$link_title = sprintf( $title, $author );?>
					<div class="flex shrink-0 text-[12px] lg:text-[18px] font-bold gap-3 items-center">
						By:

						<div class="flex gap-2 items-center">
						<?php include(locate_template('assets/img/icon-user.svg'));?>
						<?php
						// Only wrap in link if profile exists
						if ( empty( $anonymous ) && bbp_user_has_profile( $user_id ) ) {
							$author_link = sprintf( '<a href="%1$s" >%4$s</a>', esc_url( $author_url ), $link_title, ' class="bbp-author-link"', $author );?>
							<?php echo $author_link;?>
						<?php }?>
						</div>
					</div>

					<div class="flex-1 flex items-center justify-between space-x-5">
						<p class="font-medium text-[12px] lg:text-[16px] lg:pt-0.5">
							<?php 
							$now_date = get_the_date('j F Y',bbp_get_topic_id());
							$now_time = get_the_date('g:i:s A',bbp_get_topic_id());
							//echo bbp_get_topic_post_date(bbp_get_topic_id(), false, true);//bbp_topic_freshness_link(); ?>
							<?php echo $now_date;?> <span class="text-blue-1">&nbsp;/&nbsp;</span> <?php echo $now_time;?>
						</p>

						<?php if ( bbp_get_topic_reply_count() > 0 ) :?>
							<a href="#replies-anchor" class="text-blue-1 text-[14px] lg:text-[18px] font-bold"><?php bbp_topic_reply_count(); ?> Replies</a>
						<?php else :?>
							<a href="#reply-anchor" class="text-blue-1 text-[14px] lg:text-[18px] font-bold">Reply</a>
						<?php endif;?>
					</div>
				</div>

			</header>

			<section class="grid gap-5 lg:gap-14 row-span-2 self-start lg:pt-3">

				<div class="flex gap-2 items-center">
					<?php 
					// show resolved status
					bb_check_resolved(bbp_get_topic_id());?>

					<div class="text-[12px] lg:text-[16px] font-medium whitespace-nowrap">
						<?php
						$resolved_reply = get_post_meta( get_the_ID(), 'resolved_reply', true );
						$resolved_reply_date = get_post_meta( get_the_ID(), 'resolved_reply_date', true );
						$resolved_reply_time = get_post_meta( get_the_ID(), 'resolved_reply_time', true );?>
						<?php if ( $resolved_reply ) :?>
							<p class="text-green-1 mb-0.5">Issue Closed with Resolution</p>
							<?php echo $resolved_reply_date;?> <span class="text-blue-1">&nbsp;/&nbsp;</span> <?php echo $resolved_reply_time;?>
						<?php endif;?>
					</div>
				</div>

				<section class="space-y-5 lg:space-y-12 ">
					<div class="bg-red-to-blue-220 p-0.5 lg:p-1 aside-clip">
						<div class="bg-grey-1 p-4 lg:p-7 aside-clip-inner">
							<header class="space-y-3 lg:space-y-6">
								<h3 class="head-14 lg:head-28">Vehicle Data</h3>
								<hr class="bg-red-to-blue"/>
							</header>
							<div class="text-[14px] lg:text-[18px] font-bold space-y-5 lg:space-y-6 pt-5 lg:pt-7">
								<p>2013 GMC Terrain Denali</p>

								<dl class="grid grid-cols-[auto_auto] gap-6 gap-y-5">
									<dt class="text-grey-2 font-medium pt-0.5 text-[12px] lg:text-[16px] text-right">VIN</dt>
									<dd>2GKFLZEK2D6177585</dd>
									<dt class="text-grey-2 font-medium pt-0.5 text-[12px] lg:text-[16px] text-right">Engine</dt>
									<dd>2.4L L4 DOHC 16V</dd>
									<dt class="text-grey-2 font-medium pt-0.5 text-[12px] lg:text-[16px] text-right">Trans</dt>
									<dd>6-speed Automatic Transaxle (Electronic)</dd>
									<dt class="text-grey-2 font-medium pt-0.5 text-[12px] lg:text-[16px] text-right">Delivery</dt>
									<dd>Fuel Injection</dd>
									<dt class="text-grey-2 font-medium pt-0.5 text-[12px] lg:text-[16px] text-right">Affected</dt>
									<dd>Starter</dd>
									<dt class="text-grey-2 font-medium pt-0.5 text-[12px] lg:text-[16px] text-right">Conditions</dt>
									<dd>Intermittant</dd>
								</dl>

							</div>
						</div>
					</div>
				</section>

				<section class="space-y-5 lg:space-y-12 ">
					<div class="bg-red-to-blue-220 p-0.5 lg:p-1 aside-clip">
						<div class="bg-grey-1 p-4 lg:p-7 aside-clip-inner">
							<header class="space-y-3 lg:space-y-6">
								<h3 class="head-14 lg:head-28">Message Controls</h3>
								<hr class="bg-red-to-blue"/>
							</header>
							<div class="text-[14px] lg:text-[18px] font-bold space-y-5 lg:space-y-6 pt-5 lg:pt-7">
								<ul class="grid max-md:grid-cols-[auto_auto] gap-5 gap-y-2">
									<li><a class="text-link text-link--blue cursor-not-allowed">Save to my Congress</a></li>
									<li class="max-md:text-right"><a class="text-link text-link--blue cursor-not-allowed">Notify me of updates</a></li>
								</ul>

							</div>
						</div>
					</div>
				</section>

			</section>
					
			<div class="body-content space-y-7 lg:space-y-14">
				<?php
				// intro
				//$customer_comments = get_post_meta( get_the_ID(), 'customer_comments', true );
				$customer_comments = get_the_content();?>

				<div class="wysiwyg">
					<?php echo wpautop($customer_comments);?>

					<?php
					// check if current users post
					if ( $topic_author_id == $current_author_id || current_user_can( 'manage_options' ) ) :?>
					<a href="<?php the_permalink();?>?edit" class="text-link mt-5 inline-block" title="Edit">
						Edit
					</a>
					<?php endif;?>
				</div>

				<aside class="">

					<div class="space-y-5 lg:space-y-7">
						<h3 class="head-22 head-28">Related Repair History</h3>
						<hr class="bg-red-to-blue"/>
					</div>

					<ul>
						<li>
							<article class="flex justify-between space-x-5 py-3 lg:py-6 items-center">
								<div class="flex gap-4 md:flex-wrap gap-x-16">
									<h4 class="head-16 lg:head-18 lg:pt-0.5">Replaced Starter Motor</h4>

									<p class="font-medium text-[12px] lg:text-[16px] lg:pt-0.5 whitspace-nowrap">
										<?php 
										$now_date = get_the_date('j F Y',bbp_get_topic_id());
										$now_time = get_the_date('g:i:s A',bbp_get_topic_id());
										//echo bbp_get_topic_post_date(bbp_get_topic_id(), false, true);//bbp_topic_freshness_link(); ?>
										<?php echo $now_date;?> <span class="text-blue-1">&nbsp;/&nbsp;</span> <?php echo $now_time;?>
									</p>
								</div>
								<a class="cursor-not-allowed shrink-0"><?php include(locate_template('assets/img/icon-arrow-right.svg'));?></a>
							</article>
							<hr class="bg-red-to-blue"/>
						</li>
					</ul>
				</aside>


				<div class="flex gap-4 items-start">
					<aside class="grid gap-3 items-center">
						<div class="flex gap-2 items-center text-[12px] lg:text-[18px] font-bold">
							<?php include(locate_template('assets/img/icon-user.svg'));?>
							<?php
							// Only wrap in link if profile exists
							if ( empty( $anonymous ) && bbp_user_has_profile( $user_id ) ) {
								$author_link = sprintf( '<a href="%1$s" >%4$s</a>', esc_url( $author_url ), $link_title, ' class="bbp-author-link"', $author );?>
								<?php echo $author_link;?>
							<?php }?>
						</div>
						<div class="text-[14px] lg:text-[16px] font-medium pl-[15px] ml-2">
							<p>Owner/Technician<br/>David Owens Auto Services, Inc.<br/>Portland, Oregon</p>
						</div>
					</aside>

					<a href="#new-post" class="btn-clip ml-auto">Reply</a>
				</div>

			</div>
		</div>


		<div class="hidden text-[12px] text-[#A4A8AC] font-semibold px-4 py-5 flex gap-3 flex-wrap select-none items-center">
			<?php //echo bbp_get_topic_favorite_link();?>
			<div class="flex space-x-2 items-center shrink-0">
				<span><img src="<?php echo THEMEURL;?>/assets/img/bbpress/like.svg" alt="Like"/></span>
				<span>Like</span>
			</div>
			<div class="flex space-x-2 items-center shrink-0">
				<span><img src="<?php echo THEMEURL;?>/assets/img/bbpress/comment.svg" alt="Comment"/></span>
				<span>Comment</span>
			</div>
			<div class="flex space-x-2 items-center shrink-0">
				<span><img src="<?php echo THEMEURL;?>/assets/img/bbpress/share.svg" alt="Share"/></span>
				<span>Share</span>
			</div>
		</div>

		

	</article>

</div>





