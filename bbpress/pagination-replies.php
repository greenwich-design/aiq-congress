<?php

/**
 * Pagination for pages of replies (when viewing a topic)
 *
 * @package bbPress
 * @subpackage Theme
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

do_action( 'bbp_template_before_pagination_loop' ); ?>

<div class="bbp-pagination float-none relative">
	<div id="replies-anchor" class="absolute -top-[var(--nav-height)] -top-[var(--nav-height-lg)]"></div>
	
	<div class="bbp-pagination-count head-18 lg:head-28"><?php //bbp_get_topic_pagination_count();  ?><?php bbp_topic_reply_count(); ?> Replies Received</div>
	<div class="bbp-pagination-links"><?php bbp_topic_pagination_links(); ?></div>
    <div class="clear-both"></div>
</div>

<?php do_action( 'bbp_template_after_pagination_loop' );
