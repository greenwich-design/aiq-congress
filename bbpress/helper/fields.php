<?php
function tagsInput($label, $name, $currentValue = '') {
    ?>
    <div class="field-group !table-row [&>*]:pb-3 [&>*]:last:pb-0">
        <div class="table-cell pr-4">
            <label for="<?php echo $name;?>" class="whitespace-nowrap"><?php echo $label;?>:</label>
        </div>
        <div class="table-cell w-full ">
            <input id="<?php echo $name;?>" class="input-tags" name="<?php echo $name;?>" value="" data-tags="<?php echo $currentValue;?>"/>
        </div>
    </div>
    <?php 
}

function formInput($label, $name, $valtype = '', $errorMsg = '', $currentValue = '') {
    ?>
    <div class="field-group !table-row [&>*]:pb-3 [&>*]:last:pb-0">
        <div class="table-cell pr-4">
            <label for="<?php echo $name;?>" class="whitespace-nowrap"><?php echo $label;?>:</label>
        </div>
        <div class="table-cell w-full">
            <input <?php echo ( $valtype == 'required' ) ? 'required' : null;?> name="<?php echo $name;?>" type="text" value="<?php echo ( $currentValue ) ? $currentValue : '';?>" data-errormsg="<?php echo $errorMsg;?>"/>
        </div>
    </div>
    <?php 
}

function formSelect($label, $name, $options, $currentValue = '') {
    ?>
    <div class="field-group !table-row [&>*]:pb-3 [&>*]:last:pb-0">
        <div class="table-cell pr-4">
            <label for="<?php echo $name;?>" class="whitespace-nowrap"><?php echo $label;?>:</label>
        </div>
        <div class="table-cell w-full">
            <div class="fake-select">
                <?php if ( $options && is_array($options) ) :?>
                <span class="fake-select-value"><?php 
                    if ( $currentValue ) :
                        echo $currentValue;
                    else :
                        echo $options[0];
                    endif;?></span>
                <?php endif;?>
                <select name="<?php echo $name;?>">

                    <?php if ( $options ) :
                    foreach ( $options as $option ) :
                        $selected = '';
                        if ( $currentValue == $option ) :
                            $selected = 'selected';
                        endif;
                        echo '<option value="'.$option.'" '.$selected.'>'.$option.'</option>';
                    endforeach; endif;?>
                    
                </select>
            </div>
        </div>
    </div>
    <?php 
}

function formRadio($label, $name, $options, $currentValue = '') {
    ?>
    <div class="field-group !table-row [&>*]:pb-3 [&>*]:last:pb-0 [&>*]:pt-3">
        <div class="table-cell pr-4">
            <label class="whitespace-nowrap">Customer Questioned?</label>
        </div>
        <div class="table-cell w-full">
            <div class="fake-radios">
                <div class="fake-radio relative space-x-4 flex">

                    <?php if ( $options && is_array($options) ) :
                        foreach ( $options as $option ) :?>
                        <label class="flex space-x-2 pb-3 cursor-pointer">
                            <div class="flex row-reverse">
                                <?php 
                                $checked = '';
                                if ( $option == $currentValue ) :
                                $checked = 'checked';
                                endif;?>

                                <input <?php echo $checked;?> name="<?php echo $name;?>" class="peer absolute invisible" type="radio" value="<?php echo $option;?>" />
                                <span class="fake-radio-span peer-checked:before:bg-[#1096FF]"></span>
                            </div>
                            <span class="text-[#1C2F75] text-[17px] font-normal"><?php echo $option;?></span>
                        </label>
                        <?php endforeach;?>
                    <?php endif;?>

                </div>
            </div>
        </div>
    </div>
    <?php 
}

function get_remote_file_info($url) {
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, TRUE);
    curl_setopt($ch, CURLOPT_NOBODY, TRUE);
    $data = curl_exec($ch);
    $fileSize = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
    $httpResponseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    return [
        'fileExists' => (int) $httpResponseCode == 200,
        'fileSize' => (int) $fileSize
    ];
}

function formTextareaUpload($label, $name, $placeholder = '', $valtype = '', $errorMsg = '', $copy = '', $files = '') {
    ?>
    <div class="field-group !table-row [&>*]:pb-3 [&>*]:last:pb-0">
        <div class="table-cell pr-4">
            <label for="<?php echo $name;?>" class="whitespace-nowrap"><?php echo $label;?>:</label>
        </div>
        <div class="table-cell w-full dropzone-wrap" data-root="<?php echo THEMEURL;?>">
            <textarea <?php echo ( $valtype == 'required' ) ? 'required' : null;?> data-errormsg="<?php echo $errorMsg;?>" name="<?php echo $name;?>" type="text" placeholder="<?php echo $placeholder;?>"><?php echo $copy;?></textarea>
            
            <div data-dropzone="<?php echo $name;?>" class="dropzone closed media media-dropzone !bg-[#FBFBFC] !border !border-t-0 !border-[#E9EAED] !text-[#1C2F75] !text-[18px] !font-semibold !text-[#ADB6BF]" id="ac-reply-post-media-uploader-<?php bp_activity_id(); ?>">
                <div class="w-[45px] mx-auto"><img src="<?php echo THEMEURL;?>/assets/img/bbpress/media.svg" alt="Media" width="45" height="46"/></div>
            </div>

            <div data-fieldname="<?php echo $name;?>-files[]" class="media-dropzone-inputs">
                <?php 
                


                if ( $files ) :
                    foreach ( $files as $file ) :
                        $filesize = get_remote_file_info($file);
                        echo '<input type="hidden" name="'.$name.'-files[]'.'" value="'.$file.'" data-filesize="'.$filesize['fileSize'].'"/>';
                    endforeach;
                endif;?>
            </div>

        </div>
    </div>
    <?php 
}

function formTextarea($label, $name, $placeholder = '', $valtype = '', $errorMsg = '', $currentValue = '') {
    ?>
    <div class="field-group !table-row [&>*]:pb-3 [&>*]:last:pb-0">
        <div class="table-cell pr-4">
            <label for="<?php echo $name;?>" class="whitespace-nowrap"><?php echo $label;?>:</label>
        </div>
        <div class="table-cell w-full">
            <textarea <?php echo ( $valtype == 'required' ) ? 'required' : null;?> data-errormsg="<?php echo $errorMsg;?>" name="<?php echo $name;?>" type="text" placeholder="<?php echo $placeholder;?>"><?php echo $currentValue;?></textarea>
            
            

        </div>
    </div>
    <?php 
}


function formDatePicker($label, $name, $currentValue = '') {
    ?>
    <div class="field-group !table-row [&>*]:pb-3 [&>*]:last:pb-0">
        <div class="table-cell pr-4">
            <label for="<?php echo $name;?>" class="whitespace-nowrap"><?php echo $label;?>:</label>
        </div>
        <div class="table-cell w-full">
            <div class="flex space-x-4 items-center">
                <input value="<?php echo $currentValue;?>" name="<?php echo $name;?>" class="datepicker text-center !inline-block !w-auto select-none cursor-pointer" readonly type="text" placeholder="DD/MM/YY"/>
                <div>
                    <img src="<?php echo THEMEURL;?>/assets/img/bbpress/calendar.svg" width="18" height="20" alt="Calendar"/>
                </div>
            </div>
        </div>
    </div>
    <?php 
}









