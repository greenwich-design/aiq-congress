<?php

/**
 * Single Forum Content Part
 *
 * @package bbPress
 * @subpackage Theme
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

?>

<div id="bbpress-forums" class="bbpress-wrapper">

	<?php //bbp_breadcrumb(); ?>
	
	<div class="flex justify-between gap-10 py-6 flex-wrap">
		<h1 class="u-h1"><?php the_title();?></h1>

		<div class="flex flex-wrap justify-center gap-5 items-center ml-auto">
			<?php if ( is_user_logged_in() ) :?>
			<button class="btn-primary w-[245px]" data-bbmodalt="steps" >New Assistance Request</button>
			<?php endif;?>

			<div class="flex justify-end">
				<form class="flex items-center space-x-2 relative" action="<?php the_permalink();?>" method="get">
					<input value="<?php echo ( $_GET['ts'] ) ? $_GET['ts'] : '';?>" placeholder="Search..." autocomplete="off" name="ts" type="text" class="w-[245px] h-[40px] border-2 border-blue-1 p-2 pr-[50px] py-1 !outline-0 rounded-[3px]" />
					<button class="w-[30px] absolute right-2 top-1/2 -translate-y-1/2" type="submit">
						<?php include(locate_template('assets/img/icons/icon-search.svg'));?>
					</button>
				</form>
			</div>

		</div>
	</div>

	
	

	<div class="flex gap-5 items-center justify-between my-5 flex-wrap ">

		<?php if ( ! bbp_is_forum_category() && bbp_has_topics() ) : ?>

			<?php bbp_get_template_part( 'pagination', 'topics'    ); ?>

		<?php endif;?>

		<div class="pl-5 ml-auto">
			<?php bbp_forum_subscription_link(); ?>
		</div>
	</div>

	<?php do_action( 'bbp_template_before_single_forum' ); ?>

	<?php if ( post_password_required() ) : ?>

		<?php bbp_get_template_part( 'form', 'protected' ); ?>

	<?php else : ?>

		<?php //bbp_single_forum_description(); ?>

		<?php if ( bbp_has_forums() ) : ?>

			<?php bbp_get_template_part( 'loop', 'forums' ); ?>

		<?php endif; ?>

		<?php if ( ! bbp_is_forum_category()  ) : ?>


			<?php bbp_get_template_part( 'loop',       'topics'    ); ?>

			<?php bbp_get_template_part( 'pagination', 'topics'    ); ?>

			<?php //bbp_get_template_part( 'form',       'topic'     ); ?>

		<?php elseif ( ! bbp_is_forum_category() ) : ?>

			<?php bbp_get_template_part( 'feedback',   'no-topics' ); ?>

			<?php //bbp_get_template_part( 'form',       'topic'     ); ?>

		<?php endif; ?>

	<?php endif; ?>

	<?php do_action( 'bbp_template_after_single_forum' ); ?>

</div>







