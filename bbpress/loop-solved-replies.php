<?php

/**
 * Replies Loop
 *
 * @package bbPress
 * @subpackage Theme
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit; 

$resolved_reply = get_post_meta( bbp_get_topic_id(), 'resolved_reply', false );?>

<ul id="topic-<?php bbp_topic_id(); ?>-replies" class="forums  bg-red-to-blue-220 aside-clip">

	<li class="bbp-body">

                <?php 
                
                $args = array(
                    'post_type' => 'reply',
                    'post_status' => 'publish',
                    'post_parent' => bbp_get_topic_id(),
                    'post__in' => $resolved_reply
                );

                $query = new WP_Query($args);
                if ( $query->have_posts() ) :
                    while ( $query->have_posts() ) : $query->the_post();
                    $res_id = get_the_ID();
                    include(locate_template('bbpress/loop-single-reply-resolved.php'));
                    //bbp_get_template_part( 'loop', 'single-reply' );
                    unset($res_id);
                    endwhile;
                endif;

                ?>

	</li><!-- .bbp-body -->

	
</ul><!-- #topic-<?php bbp_topic_id(); ?>-replies -->