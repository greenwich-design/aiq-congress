<?php

/**
 * Single Topic Content Part
 *
 * @package bbPress
 * @subpackage Theme
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

?>

<div id="" class="md:w-11/12 mx-auto">

    <nav>
        <ul class="flex space-x-3 font-medium mb-5 lg:mb-10 items-center">
            <li><a class="text-link text-link--current !text-[16px] block" href="<?php bloginfo('url');?>">Home</a></li>
            <li class="text-blue-1  !text-[16px] block">&nbsp;/&nbsp;</li>
            <li><a class="text-link text-link--current !text-[16px] block" href="<?php bloginfo('url');?>">Help Requests</a></li>
        </ul>
    </nav>
    
    <div class="space-y-10 lg:space-y-14">
        <?php include(locate_template('bbpress/single-topic-card.php'));?>

        <?php do_action( 'bbp_template_before_single_topic' ); ?>

        <?php if ( post_password_required() ) : ?>

            <?php bbp_get_template_part( 'form', 'protected' ); ?>

        <?php else : ?>

            <div class="grid gap-7 md:gap-10 lg:gap-x-14 md:grid-cols-[auto_minmax(auto,min-content)] lg:grid-cols-[auto_minmax(396px,min-content)]">
                <div class="space-y-7 lg:space-y-10">
                <?php //bbp_topic_tag_list(); ?>

                <?php //bbp_single_topic_description(); ?>

                <?php if ( bbp_show_lead_topic() ) : ?>

                    <?php //bbp_get_template_part( 'content', 'single-topic-lead' ); ?>

                <?php endif; ?>



                <?php if ( bbp_has_replies() ) : ?>
                    
                    <?php 
                    // get solved replies
                    $resolved_reply = get_post_meta( bbp_get_topic_id(), 'resolved_reply', true );?>
                    <?php if ( $resolved_reply ) :  
                        bbp_get_template_part( 'loop', 'solved-replies' ); 
                    endif;?>

                    <hr class="bg-red-to-blue !mt-8 lg:!mt-12">

                    <?php bbp_get_template_part( 'pagination', 'replies' ); ?>

                    <?php bbp_get_template_part( 'loop',       'replies' ); ?>

                    <?php //bbp_get_template_part( 'pagination', 'replies' ); ?>

                <?php endif; ?>

                <?php bbp_get_template_part( 'form', 'reply' ); ?>
                </div>
            </div>

        <?php endif; ?>
    </div>

	<?php bbp_get_template_part( 'alert', 'topic-lock' ); ?>

	<?php do_action( 'bbp_template_after_single_topic' ); ?>

</div>
