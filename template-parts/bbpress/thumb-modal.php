<?php

$type = wp_check_filetype($file);
$uid = uniqid();

echo '<div class="pt-[100%] relative bg-white border border-[#E9EAED] rounded-lg">';
if ( $type['ext'] == 'pdf' ) :
	echo '<img class="lazyload rounded-lg absolute w-full h-full object-cover inset-0 p-5" data-src="'.THEMEURL.'/assets/img/pdf.svg" loading="lazy" alt=""/>';
	echo '<a class="absolute inset-0 w-full h-full" target="_blank" href="'.$file.'" download></a>';
else :
	echo '<img class="lazyload rounded-lg absolute w-full h-full object-contain inset-0 p-2" data-src="'.$file.'" loading="lazy" alt=""/>';
	echo '<a data-modaltrigger="'.$uid.'" class="absolute inset-0 w-full h-full" target="_blank" href="'.$file.'"></a>';
	
	?>
	<!-- modal -->    
	<div data-modal="<?php echo $uid;?>" data-modaltrigger="<?php echo $uid;?>" class="p-5 lg:p-10 noajax fixed inset-0 w-full h-full z-50 bg-black/50 flex invisible opacity-0 transition-[opacity,visibility]">
		<div class="m-auto h-full w-full relative container">
		<img class="lazyload absolute inset-0 left-1/2 top-1/2 -translate-x-1/2 -translate-y-1/2 rounded-[6px] max-h-full object-contain" data-src="<?php echo $file;?>" loading="lazy" alt=""/>';
		</div>
	</div>
	<!-- /modal -->
	<?php 
	
endif;
echo '</div>';