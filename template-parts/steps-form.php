
<form id="steps-form" class=" space-y-5 relative">
	
    <?php if ( $form_type == 'insert_topic' ) :?>
	<input type="hidden" name="parent_id" value="<?php the_ID();?>"/>
	<input type="hidden" name="_bbp_forum_id" value="<?php the_ID();?>"/>
    <?php endif;?>

	<?php if ( $form_type == 'edit_topic' && is_singular('topic') ) :?>
		<input type="hidden" name="topic_id" value="<?php the_ID();?>"/>
	<?php endif;?>
	
	<input type="hidden" name="action" value="<?php echo $form_type;?>"/>

	<?php
	$current_author = get_the_author_meta('ID');?>
	<input type="hidden" name="current_author" value="<?php echo $current_author;?>"/>
	<input type="hidden" name="_bbp_engagement" value="<?php echo $current_author;?>"/>
	<input type="hidden" name="_bbp_author_ip" value="<?php echo $_SERVER['REMOTE_ADDR'];?>"/>
		
	<div id="steps-msg"></div>

	<div class="step rounded-[5px] border-1 border-[#E9EAED] bg-white shadow-[0_0_30px_0_#E8E9EE]">
		<div class="flex px-6 py-4 items-center justify-between space-x-5 min-h-[60px]">
			<h3 class="u-h2">New Assistance Request</h3>
			<button type="button" data-stepedit class="edit text-[#16C224] font-semibold text-[12px] hidden">Edit</button>
		</div>

		<div class="step-body border-t border-[#E9EAED]">
			<div class="px-6 py-8">

				<div class="flex gap-4">
					<div class="table w-full">

						<?php
                        $currentValue = '';
                        if ( $form_type == 'edit_topic' && is_singular( 'topic' ) ) :
                        $currentValue = get_the_title( get_the_ID() );
                        endif;
                        formInput('Request Title', 'post_title', 'required', 'Please Enter value', $currentValue);?>
						
					</div>

					<div class="shrink-0 ">
						<div class="reg-check bg-[#2A4596] h-[50px] flex rounded-[12px] overflow-hidden">
							<div class="w-[75px] shrink-0 text-center self-center">
								<img class="block mx-auto" src="<?php echo THEMEURL;?>/assets/img/bbpress/eng-flag.svg" width="36" height="23" alt="UK Flag"/>
							</div>
							<div class="flex">
                                <?php
                                $currentValue = '';
                                if ( $form_type == 'edit_topic' && is_singular( 'topic' ) ) :
                                $currentValue = get_post_meta(get_the_ID(), 'reg_number', true );
                                endif;?>
								<input onkeydown="return /[a-z0-9]/i.test(event.key)" class="bg-[#EFCD31] w-[170px] outline-none text-center font-semibold text-blue-1 text-[22px] uppercase" value="<?php echo $currentValue;?>"  maxlength="8" type="text" name="reg_number"/>
							</div>
						</div>
					</div>
				</div>
				
			</div>

            <?php if ( $form_type !== 'edit_topic' ) :?>
			<div class="actions bg-[#FBFBFC] py-4 px-6 flex border-t border-[#E9EAED]">
				<div class="ml-auto">
					<button type="button" data-stepnext arial-label="Next" class="btn-primary">Next</button>
				</div>
			</div>
            <?php endif;?>

		</div>
	</div>

	<div class="step rounded-[5px] border-1 border-[#E9EAED] bg-white shadow-[0_0_30px_0_#E8E9EE]">
		<div class="flex px-6 py-4 items-center justify-between space-x-5 min-h-[60px]">
			<h3 class="u-h2 opacity-50 <?php echo ( $form_type == 'edit_topic' ) ? '!opacity-100' : '';?>">Vehicle Details</h3>
			<button type="button" data-stepedit class="edit text-[#16C224] font-semibold text-[12px] hidden">Edit</button>
		</div>

		<div class="step-body border-t border-[#E9EAED] hidden <?php echo ( $form_type == 'edit_topic' ) ? '!block' : '';?>">
			<div class="px-6 py-8">

				<div class="table w-full">
					
					<?php 
						formInput('Vehicle Make', 'vehicle_make', 'required', 'Please Enter value', get_post_meta(get_the_ID(), 'vehicle_make', true ));
						formInput('Vehicle Model', 'vehicle_model', 'required', 'Please Enter value', get_post_meta(get_the_ID(), 'vehicle_model', true ) );
						formInput('Engine Size', 'engine_size', 'required', 'Please Enter value', get_post_meta(get_the_ID(), 'engine_size', true ));
						formInput('Engine Number', 'engine_number', 'required', 'Please Enter value', get_post_meta(get_the_ID(), 'engine_number', true ));
						formSelect('Engine Size', 'engine_size', array('Automatic', 'Manual'), get_post_meta(get_the_ID(), 'engine_size', true ));
						formSelect('Fuel Type', 'fuel_type', array('Petrol', 'Diesel', 'Petrol Hybrid', 'Diesel Hybrid', 'Electric'), get_post_meta(get_the_ID(), 'fuel_type', true ));
					?>

					
				</div>		
				
			</div>

            <?php if ( $form_type !== 'edit_topic' ) :?>
			<div class="actions bg-[#FBFBFC] py-4 px-6 flex border-t border-[#E9EAED]">
				<div class="">
					<button data-stepprev arial-label="Prev" class="btn-blank" title="Prev">Prev</button>
				</div>
				<div class="ml-auto">
					<button type="button" data-stepnext arial-label="Next" class="btn-primary">Next</button>
				</div>
			</div>
            <?php endif;?>

		</div>

	</div>

	<div class="step rounded-[5px] border-1 border-[#E9EAED] bg-white shadow-[0_0_30px_0_#E8E9EE]">
		<div class="flex px-6 py-4 items-center justify-between space-x-5 min-h-[60px]">
			<h3 class="u-h2 opacity-50 <?php echo ( $form_type == 'edit_topic' ) ? '!opacity-100' : '';?>">Vehicle Situation</h3>
			<button type="button" data-stepedit class="edit text-[#16C224] font-semibold text-[12px] hidden">Edit</button>
		</div>

		<div class="step-body border-t border-[#E9EAED] hidden <?php echo ( $form_type == 'edit_topic' ) ? '!block' : '';?>">
			<div class="px-6 py-8">

				<div class="table w-full">

					<?php 
					formRadio('Customer Questioned?', 'customer_questioned', array('Yes', 'No'), get_post_meta(get_the_ID(), 'customer_questioned', true ) );
					
                    $copy = get_post_meta(get_the_ID(), 'customer_comments', true );
                    $files = get_post_meta(get_the_ID(), 'customer_comments-files', false );
                    formTextareaUpload('Customer Comments', 'customer_comments', 'Please enter the customers statement…', '', '', $copy, $files);?>
					
					<div class="table-row">
						<div class="table-cell pr-4">
							<h4 class="u-h4 mt-10 mb-5">Fault Details</h4>
						</div>
					</div>
					
					<?php 
					formDatePicker('First occured', 'fd_first_occured', get_post_meta(get_the_ID(), 'fd_first_occured', true ) );
					formDatePicker('Recent occurance', 'fd_recent_occurance', get_post_meta(get_the_ID(), 'fd_recent_occurance', true ));
					formDatePicker('Time before that', 'fd_time_before', get_post_meta(get_the_ID(), 'fd_time_before', true ));?>
					
					
					<div class="table-row">
						<div class="table-cell pr-4">
							<h4 class="u-h4 mt-10 mb-5">Fault Conditions</h4>
						</div>
					</div>
					
					
					<?php 
                    $copy = get_post_meta(get_the_ID(), 'fault_conditions', true );
                    $files = get_post_meta(get_the_ID(), 'fault_conditions-files', false );
					formTextareaUpload('Conditions', 'fault_conditions', 'Please enter the fault conditions…', 'required', 'Please enter Conditions', $copy, $files);
					formRadio('Was there a change point?', 'change_point_radio', array('Yes', 'No'), get_post_meta(get_the_ID(), 'change_point_radio', true ));
					$copy = get_post_meta(get_the_ID(), 'change_point_desc', true );
                    $files = get_post_meta(get_the_ID(), 'change_point_desc-files', false );
                    formTextareaUpload('Change point comments', 'change_point_desc', 'Please describe…', '', '', $copy, $files);?>
					
				</div>		
				
			</div>

            <?php if ( $form_type !== 'edit_topic' ) :?>
			<div class="actions bg-[#FBFBFC] py-4 px-6 flex border-t border-[#E9EAED]">
				<div class="">
					<button data-stepprev arial-label="Prev" class="btn-blank" title="Prev">Prev</button>
				</div>
				<div class="ml-auto">
					<button type="button" data-stepnext arial-label="Next" class="btn-primary">Next</button>
				</div>
			</div>
            <?php endif;?>

		</div>

	</div>

	<div class="step rounded-[5px] border-1 border-[#E9EAED] bg-white shadow-[0_0_30px_0_#E8E9EE]">
		<div class="flex px-6 py-4 items-center justify-between space-x-5 min-h-[60px]">
			<h3 class="u-h2 opacity-50 <?php echo ( $form_type == 'edit_topic' ) ? '!opacity-100' : '';?>">Technician Details</h3>
			<button type="button" data-stepedit class="edit text-[#16C224] font-semibold text-[12px] hidden">Edit</button>
		</div>

		<div class="step-body border-t border-[#E9EAED] hidden <?php echo ( $form_type == 'edit_topic' ) ? '!block' : '';?>">
			<div class="px-6 py-8">

				<div class="table w-full">

					<?php formInput('Job Number', 'job_number', '', '', get_post_meta(get_the_ID(), 'job_number', true ));?>

					<div class="table-row">
						<div class="table-cell pr-4">
							<h4 class="u-h4 mt-10 mb-5">Technician Comments</h4>
						</div>
					</div>

					<?php 
                    $copy = get_post_meta(get_the_ID(), 'technician_comments', true );
                    $files = get_post_meta(get_the_ID(), 'technician_comments-files', false );
                    formTextareaUpload('Technician Comments', 'technician_comments', '', '', '', $copy, $files);?>
					<?php formInput('Fault Code', 'fault_code', '', '', get_post_meta(get_the_ID(), 'fault_code', true ));?>
					<?php tagsInput('Affected Systems', 'affected_systems', get_post_meta(get_the_ID(), 'affected_systems', true ));?>
					<?php formTextarea('TSB Consultation Notes', 'tsb_consultation_notes', '', '', '', get_post_meta(get_the_ID(), 'tsb_consultation_notes', true ));?>
					
					
					
				</div>		
				
			</div>

			<div class="actions bg-[#FBFBFC] py-4 px-6 flex border-t border-[#E9EAED]">
				<div class="">
					<?php if ( $form_type == 'edit_topic' && is_singular('topic') ) :?>
					<a href="<?php the_permalink();?>" arial-label="Prev" class="edit-btn btn-blank" title="Cancel">Cancel</a>
					<?php else :?>
						<button data-stepprev arial-label="Prev" class="btn-blank" title="Prev">Prev</button>
					<?php endif;?>
				</div>
				<div class="ml-auto">
					<button type="button" data-stepsubmit arial-label="Post Assistance Request" class="btn-primary"><?php
					if ( $form_type == 'edit_topic' && is_singular('topic') ) :
						echo 'Submit Edit';
					else :
						echo 'Post Assistance Request';
						
					endif;?></button>
				</div>
			</div>
		</div>

	</div>

</form>