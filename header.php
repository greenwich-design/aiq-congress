<!DOCTYPE html>
<html lang="en" class="overflow-x-hidden scroll-smooth">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php wp_title('-', true,''); ?></title>

    <?php wp_head(); ?>
    <link
  rel="stylesheet"
  href="https://unpkg.com/dropzone@5/dist/min/dropzone.min.css"
  type="text/css"
/>
</head>

<?php

$body_class = 'bg-grey-1 bg-[url(assets/img/main-bg-hex.svg)] bg-no-repeat bg-top max-md:bg-[length:auto_50%]';

if( is_user_logged_in() ) {
  $user = wp_get_current_user();
  $roles = $user->roles;
  if ( is_array($roles) ) {
    $body_class .= implode(' ', $roles);
  } else {
    $body_class .= $roles;
  }
}

$body_class .= ' text-[16px] lg:text-[20px] leading-[1.325] text-copy flex flex-col min-h-screen transition-colors';?>
<body <?php body_class($body_class); ?>>

  <header id="header" class="sticky z-50 top-0 left-0 mb-5 lg:mb-0 h-[var(--nav-height)] lg:h-[var(--nav-height-lg)] background-blend-darken bg-[image:var(--nav-grad)] md:bg-[image:var(--nav-grad-md)]">
    <div class="container flex space-x-5 items-start">

      <div class="py-4 lg:py-11">
        <a class="flex max-lg:flex-col gap-[2px] lg:gap-[10px] items-start" href="<?php bloginfo('url');?>">
          <img class="max-md:w-[61px] md:max-lg:w-[90px] h-auto" src="<?php echo THEMEURL;?>/assets/img/autoiq-logo.svg" width="137" height="35" alt="<?php bloginfo('name');?>"/>
        </a>
      </div>

      <?php if ( is_user_logged_in() ) :?>
      <div class="text-[16px] lg:text-[24px] max-md:hidden flex h-[75px] lg:h-[105px] font-medium text-white group">

        <div class="absolute pointer-events-none opacity-0">
        <?php
        $withClip = true;
        include(locate_template('assets/img/head-btn-share-solution.svg'));
        include(locate_template('assets/img/head-btn-request.svg'));
        unset($withClip);
        ?>
        </div>

        <button data-bbmodalt="steps" class="will-change-transform transition-transform origin-top group-hover:scale-[.975] hover:!scale-[1.025] ease-in-out relative h-full" style="clip-path:url(#clipRequest)"><?php include(locate_template('assets/img/head-btn-request.svg'));?><span class="absolute inset-0 flex items-center justify-center">Request Assistance</span></button>
        <button class="will-change-transform transition-transform origin-top group-hover:scale-[.975] hover:!scale-[1.025] ease-in-out relative h-full -ml-16 lg:-ml-10" style="clip-path:url(#clipShare)"><?php include(locate_template('assets/img/head-btn-share-solution.svg'));?><span class="absolute inset-0 flex items-center justify-center">Share Solution</span></button>
      </div>
      <?php endif;?>
      
      <ul class="hidden text-[14px] flex flex-wrap gap-5 text-blue-1 font-bold">
      <?php
      $nav = wp_nav_menu(
          array(

              'theme_location' => 'header',
              'container' => false,
              'container_id' => '',
              'container_class' =>' ',
              'menu_id' => false,
              'items_wrap' => '%3$s',
              'fallback_cb' => false,
              'depth' => 0,
              'after' => '',
              'link_after' => '',
              'echo'  => false
          )
      );
      echo $nav;?>
      </ul>
    </div>
  </header>

        

        
