<?php

/**
* Global vars
*/
define('THEMEURL', get_stylesheet_directory_uri());
define('THEMEPATH', get_stylesheet_directory());

/**
 * Theme version to version scripts and styles
 */
define('THEME_VERSION', wp_get_theme()->get('Version'));

include(THEMEPATH.'/inc/custom-ajax-auth.php');

include(THEMEPATH.'/bbpress/helper/fields.php');
include(THEMEPATH.'/bbpress-func.php');

/**
* Theme Setup
*/
function theme_setup() {
    /**
    * Featured Image Support
    */
    add_theme_support('post-thumbnails');
    

    
    add_image_size( 'thumb-100', 100);    
    add_image_size( 'thumb-150', 150);    
    add_image_size( 'thumb-300', 300);    
    add_image_size( 'thumb-600', 600);    
        
    

    /**
    * Nav Menus
    */
    register_nav_menus(array(
        'header' => __('Header Main'),
        'footer' => __('Footer Main')        
    ));
}



add_action('after_setup_theme', 'theme_setup');

@ini_set( 'upload_max_size' , '64M' );
@ini_set( 'post_max_size', '64M');
@ini_set( 'max_execution_time', '300' );


add_editor_style( 'editor-style.css' );


/**
 * Enqueue scripts and styles
 */
function sites_scripts() {
    // CSS files

    
    wp_enqueue_style('style', get_template_directory_uri() . '/style.css', '', THEME_VERSION);

    wp_enqueue_script('lazyload-js', get_template_directory_uri() . '/scripts/lib/lazy-load.min.js', array(), THEME_VERSION, true);

    wp_enqueue_script('app-js', get_template_directory_uri() . '/scripts/site/app.min.js', array(), THEME_VERSION, true);
    wp_localize_script( 'app-js', 'myObj', array(
        'ajaxUrl' => admin_url( 'admin-ajax.php' )
    ) );

    wp_enqueue_script('jquery-ui-datepicker');
    wp_enqueue_script('bbpress-js', get_template_directory_uri() . '/scripts/site/bbpress.min.js', array('jquery-ui-datepicker'), THEME_VERSION, true);
    wp_localize_script( 'bbpress-js', 'myObj', array(
        'ajaxUrl' => admin_url( 'admin-ajax.php' ),
        'security' => wp_create_nonce( 'file_upload' )
    ) );

    wp_enqueue_script('dropzone-js', 'https://unpkg.com/dropzone@5/dist/min/dropzone.min.js', array(), THEME_VERSION, true);
    wp_enqueue_script('dropzone-config-js', get_template_directory_uri() . '/scripts/site/dropzone-config.min.js', array('dropzone-js'), THEME_VERSION, true);
    wp_localize_script( 'dropzone-config-js', 'myObj', array(
        'ajaxUrl' => admin_url( 'admin-ajax.php' ),
        'security' => wp_create_nonce( 'file_upload' )
    ) );

    wp_enqueue_style('e2b-admin-ui-css','http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/themes/base/jquery-ui.css',false,"1.9.0",false);

    
    /**
     * Move jquery to footer
     */
    wp_scripts()->add_data( 'jquery', 'group', 1 );
    wp_scripts()->add_data( 'jquery-core', 'group', 1 );
    wp_scripts()->add_data( 'jquery-migrate', 'group', 1 );

    /**
     * Dequeue Gutenburg Assets
     */
    wp_dequeue_style( 'wp-block-library' ); // WordPress core
    wp_dequeue_style( 'wp-block-library-theme' ); // WordPress core
    wp_dequeue_style( 'wc-block-style' ); // WooCommerce
    wp_dequeue_style( 'storefront-gutenberg-blocks' ); // Storefront theme


}
add_action( 'wp_enqueue_scripts', 'sites_scripts', 100 );

function header_stuff() {
    // add header fonts, scripts, etc
    ?>
    <link rel="preconnect" href="https://use.typekit.net" crossorigin />

    <!-- We use the full link to the CSS file in the rest of the tags -->
    <link rel="preload" as="style" href="https://use.typekit.net/jdr8jjm.css" />
    <link rel="stylesheet" href="https://use.typekit.net/jdr8jjm.css" media="print" onload="this.media='all'" />
    <noscript>
        <link rel="stylesheet" href="https://use.typekit.net/jdr8jjm.css" />
    </noscript>
    <?php
}
add_action('wp_head', 'header_stuff');

add_action('wp_ajax_file_upload', 'file_upload_callback');
add_action('wp_ajax_nopriv_file_upload', 'file_upload_callback');
function file_upload_callback() {
    check_ajax_referer('file_upload', 'security');
    $arr_img_ext = array('image/png', 'image/jpeg', 'image/jpg', 'image/gif');
    //if (in_array($_FILES['file']['type'], $arr_img_ext)) {
        $upload = wp_upload_bits($_FILES["file"]["name"], null, file_get_contents($_FILES["file"]["tmp_name"]));
        //$upload['url'] will gives you uploaded file path

        $json = array(
            'success' => true
        );

        $result = array_merge($upload, $json);
        echo json_encode($result);

    //} else {
    //    wp_send_json_error();
    //}
    
    wp_die();
}



// insert "topic" post
add_action('wp_ajax_insert_topic', 'wpse_126886_ajax_handler');
add_action('wp_ajax_nopriv_insert_topic', 'wpse_126886_ajax_handler');

function wpse_126886_ajax_handler() {

        
    // maybe check some permissions here, depending on your app
    if ( ! current_user_can( 'subscriber' ) && !is_user_logged_in() ) :
            
            // send some information back to the javascipt handler
            $response = array(
                'status' => '401',
                'message' => 'You must be logged in to do that.'
            );

            // normally, the script expects a json respone
            header( 'Content-Type: application/json; charset=utf-8' );
            echo json_encode( $response );
        exit;
    endif;
    
    $post_title = $_POST['post_title'];
    $parent_id = $_POST['parent_id'];
    $post_content = $_POST['customer_comments'];
    
    
    $post_data = array(
        'post_type'     => 'topic',
        'post_title'    => $post_title,
        'post_author'   => $current_author,
        'post_parent'   => $parent_id,
        'post_content'  => $post_content,
        'post_status'   => 'publish'
    );
    //handle your form data here by accessing $_POST

    $new_post_ID = wp_insert_post( $post_data );

    // send some information back to the javascipt handler
    $response = array(
        'status' => '200',
        'message' => 'OK',
        'new_post_ID' => $new_post_ID
    );

    foreach ( $_POST as $key => $val ) :
        if ( is_array($val) ) :

            foreach ( $val as $child_key => $child_val ) :
                add_post_meta( $new_post_ID, $key, $child_val, false );
            endforeach;
        else :
            add_post_meta( $new_post_ID, $key, $val, false );
        endif;

    endforeach;


    add_post_meta( $new_post_ID, '_bbp_topic_id', $new_post_ID, false );
    add_post_meta( $new_post_ID, '_bbp_voice_count', '1', false );
    add_post_meta( $new_post_ID, '_bbp_last_active_time', date('Y-m-d H:i:s'), false );

    // normally, the script expects a json respone
    header( 'Content-Type: application/json; charset=utf-8' );
    echo json_encode( $response );

    exit; // important
}

// insert "topic" post
add_action('wp_ajax_edit_topic', 'wpse_126885_ajax_handler');
add_action('wp_ajax_nopriv_edit_topic', 'wpse_126885_ajax_handler');

function wpse_126885_ajax_handler() {

    // maybe check some permissions here, depending on your app
    $user = wp_get_current_user();
    if ( (!in_array( 'subscriber', (array) $user->roles ) && !in_array( 'administrator', (array) $user->roles )) && ! current_user_can( 'edit_posts' ) ) :
            
            // send some information back to the javascipt handler
            $response = array(
                'status' => '401',
                'message' => 'You must be logged in to do that.'
            );

            // normally, the script expects a json respone
            header( 'Content-Type: application/json; charset=utf-8' );
            echo json_encode( $response );
        exit;
    endif;
    
    $post_title = $_POST['post_title'];
    $post_content = $_POST['customer_comments'];
    $id = $_POST['topic_id'];
    
    $post_data = array(
        'ID'     => $id,
        'post_title'    => $post_title,
        'post_content'  => $post_content
    );
    //handle your form data here by accessing $_POST

    $new_post_ID = wp_update_post( $post_data );

    // send some information back to the javascipt handler
    $response = array(
        'status' => '200',
        'message' => 'OK',
        'new_post_ID' => $new_post_ID
    );

    foreach ( $_POST as $key => $val ) :
        if ( is_array($val) ) :
            delete_post_meta( $new_post_ID, $key );
            foreach ( $val as $child_key => $child_val ) :
                add_post_meta( $new_post_ID, $key, $child_val, false );
            endforeach;
        else :
            delete_post_meta( $new_post_ID, $key );
            add_post_meta( $new_post_ID, $key, $val, false );
        endif;

    endforeach;

    // normally, the script expects a json respone
    header( 'Content-Type: application/json; charset=utf-8' );
    echo json_encode( $response );

    exit; // important
}

add_action('wp_ajax_get_updated_topic', 'aj_get_updated_topic');
add_action('wp_ajax_nopriv_get_updated_topic', 'aj_get_updated_topic');

function aj_get_updated_topic() {
    if ( ! $_POST['post_id'] )
        exit;

    $args = array(
        'post_type' => 'topic',
        'posts_per_page' => 1,
        'post__in' => array($_POST['post_id'])
    );

    // send some information back to the javascipt handler
    $response = array(
        'status' => '200',
        'message' => 'OK',
        'html'  => '',
        'html_header'  => '',
        'html_footer'  => ''
    );

    $query = new WP_Query($args);
    if ( $query->have_posts() ) :
        while ( $query->have_posts() ) : $query->the_post();
        
        // header
        ob_start();?>
        
        <div class="text-[22px] text-[#28C635] font-semibold px-6 py-5 rounded-[5px] bg-white shadow-[0_0_26px_0_#E8E9EE] relative mb-5">
            <p>Request Created Successfully</p>
        </div>

        <?php 
        $response['html_header'] = ob_get_clean();

        // body
        ob_start();?>
        
        <?php include(locate_template('bbpress/topic-card.php'));?>

        <?php 
        $response['html'] = ob_get_clean();

        // footer
        ob_start();?>
        <div class="actions bg-[#FBFBFC] py-4 px-6 flex border-t border-[#E9EAED]">
            <div class="">
            </div>
            <div class="ml-auto">
                <a href="<?php the_permalink();?>" arial-label="Post Assistance Request" class="btn-primary">View Full Post</a>
            </div>
        </div>
        
        <?php
        $response['html_footer'] = ob_get_clean();
        endwhile;
    endif;
    
    // normally, the script expects a json respone
    header( 'Content-Type: application/json; charset=utf-8' );
    echo json_encode( $response );

    exit; // important
}

add_action('wp_ajax_mark_topic_solved', 'wpse_126887_ajax_handler');
add_action('wp_ajax_nopriv_mark_topic_solved', 'wpse_126887_ajax_handler');

function wpse_126887_ajax_handler() {

    // maybe check some permissions here, depending on your app
    if ( ! current_user_can( 'subscriber' ) && !is_user_logged_in() ) :
            
            // send some information back to the javascipt handler
            $response = array(
                'status' => '401',
                'message' => 'You must be logged in to do that.'
            );

            // normally, the script expects a json respone
            header( 'Content-Type: application/json; charset=utf-8' );
            echo json_encode( $response );
        exit;
    endif;
    
    $replyid = $_POST['replyid'];
    $topicid = $_POST['topicid'];
    $marksolved = $_POST['marksolved'];
    
    if ( $marksolved == 'false' ) :
        $meta = delete_post_meta( $topicid, 'resolved_reply', $replyid);
        $meta2 = delete_post_meta( $topicid, 'resolved_reply_date');
        $meta3 = delete_post_meta( $topicid, 'resolved_reply_time');
    else :
        $meta = add_post_meta( $topicid, 'resolved_reply', $replyid, false );
        $now_date = date('j F Y');
        $now_time = date('g:i:s A');
        $meta2 = add_post_meta( $topicid, 'resolved_reply_date', $now_date, false );
        $meta3 = add_post_meta( $topicid, 'resolved_reply_time', $now_time, false );
    endif;

    if ( $meta ) :
        // send some information back to the javascipt handler
        $response = array(
            'status' => '200',
            'message' => 'OK'
        );
    else :
       // send some information back to the javascipt handler
       $response = array(
        'status' => '500',
        'message' => 'ERROR'
       ); 
    endif;

    // normally, the script expects a json respone
    header( 'Content-Type: application/json; charset=utf-8' );
    echo json_encode( $response );

    exit; // important
}


// add custom fields to topics
add_post_type_support( 'topic', 'custom-fields' );

function custom_bbp_show_lead_topic( $show_lead ) {
    $show_lead[] = 'false';
    return $show_lead;
  }
   
add_filter('bbp_show_lead_topic', 'custom_bbp_show_lead_topic' );

// add the 'buddypress-activity' support!
add_post_type_support( 'topic', 'buddypress-activity' );
function customize_page_tracking_args() {
    // Check if the Activity component is active before using it.
    if ( ! bp_is_active( 'activity' ) ) {
        return;
    }
 
    bp_activity_set_post_type_tracking_args( 'topic', array(
        'component_id'             => buddypress()->activity->id,
        'action_id'                => 'new_topic',
        'bp_activity_admin_filter' => __( 'Neuer Artikel veröffentlicht', 'custom-domain' ),
        'bp_activity_front_filter' => __( 'Topic', 'custom-domain' ),
        'contexts'                 => array( 'activity', 'member' ),
        'bp_activity_new_post'     => __( '%1$s has posted a new <a class="!underline" href="%2$s">Post</a>', 'sozialdynamik' ),
        'bp_activity_new_post_ms'  => __( '%1$s has posted a new <a class="!underline" href="%2$s">Post</a>, on the site %3$s', 'sozialdynamik' ),
        'position'                 => 100,
    ) );
}
add_action( 'bp_init', 'customize_page_tracking_args' );

//Replace login styles
function my_custom_login_stylesheet() {
    wp_enqueue_style( 'custom-login', get_stylesheet_directory_uri() . '/style.css' );
    wp_enqueue_script( 'custom-login-js', get_stylesheet_directory_uri() . '/scripts/site/login.min.js', array('jquery') );
}

//This loads the function above on the login page
add_action( 'login_enqueue_scripts', 'my_custom_login_stylesheet' );





// remove items from admin bar if subscriber
function remove_from_admin_bar($wp_admin_bar) {
    /*
     * Placing items in here will only remove them from admin bar
     * when viewing the fronte end of the site
    */
    $user = wp_get_current_user();
    if ( ! is_admin() && in_array( 'subscriber', (array) $user->roles ) ) {
        // Example of removing item generated by plugin. Full ID is #wp-admin-bar-si_menu
 
        // WordPress Core Items (uncomment to remove)
        $wp_admin_bar->remove_node('site-name');
        $wp_admin_bar->remove_node('search');
        $wp_admin_bar->remove_node('wp-logo');
        $wp_admin_bar->remove_node('my-sites');
        $wp_admin_bar->remove_node('edit');
        
    }
 
    /*
     * Items placed outside the if statement will remove it from both the frontend
     * and backend of the site
    */
}
add_action('admin_bar_menu', 'remove_from_admin_bar', 999);



function possibly_redirect(){
    global $pagenow;
    if( 'wp-login.php' == $pagenow ) {
      if ( isset( $_POST['wp-submit'] ) ||   // in case of LOGIN
        ( isset($_GET['action']) && $_GET['action']=='logout') ||   // in case of LOGOUT
        ( isset($_GET['checkemail']) && $_GET['checkemail']=='confirm') ||   // in case of LOST PASSWORD
        ( isset($_GET['checkemail']) && $_GET['checkemail']=='registered') ) return;    // in case of REGISTER
      else wp_redirect( home_url('/login') ); // or wp_redirect(home_url('/login'));
      exit();
    }
  }
add_action('init','possibly_redirect');


function redirect_to_specific_page() {

    if ( !is_page('register') && ! is_user_logged_in() && !is_page( 'login' ) ) {

        //auth_redirect();
        wp_redirect( home_url('/login') );
        exit;
    }
}
add_action( 'template_redirect', 'redirect_to_specific_page' );


function ref_access() {

    global $error;

    if( empty($_GET['ref']) && empty($_GET['action']) )
        $error  = 'Please login to access this website.';
}
add_action('login_head','ref_access');