
jQuery(document).ready(function() {
    
    
    jQuery('.field-group .datepicker').each(function() {
        jQuery(this).datepicker({
            dateFormat : 'dd/mm/yy'
        });
    });

    // accordion toggles
    jQuery('.bb-accordion-header').each(function() {
        var parent = jQuery(this).parents('.bb-accordion');
        parent.on('click', '.bb-accordion-header', function() {
            parent.find('.bb-accordion-body').slideToggle();

            parent.find('.bb-accordion-chev').toggleClass('rotate-180');
        });
    });



});


var bbpress;

bbpress = {

    fetchElements: async function (url, selector) {
      const data = await fetch(url).then(res => res.text());
      const parsed = new DOMParser().parseFromString(data, 'text/html');
      return parsed.querySelectorAll(selector);
    },


    stepsVal: function() {
        
        const steps = document.querySelectorAll('.step');

        if ( steps.length ) {
            
            // step next
            const stepNexts = document.querySelectorAll('[data-stepnext]');
            const stepSubmits = document.querySelectorAll('[data-stepsubmit]');

            if ( stepNexts.length ) {
                stepNexts.forEach(function(stepNext) {
                
                    var parentStep = stepNext.closest('.step');
                    
                    stepNext.addEventListener('click', function() {

                        if ( validateFields(parentStep) ) {
                            
                            if ( parentStep.querySelector('[data-stepedit]') ) {
                                parentStep.querySelector('[data-stepedit]').classList.add('!block');
                            }

                            var el = jQuery(parentStep);
                            var next = el.next('.step');

                            if ( next ) {
                                el.find('.step-body').slideUp();

                                next.find('.step-body').slideDown();
                                next.find('.u-h2').removeClass('opacity-50');
                            }
                            
                        } else {
                            if ( parentStep.querySelector('[data-stepedit]') ) {
                                parentStep.querySelector('[data-stepedit]').classList.remove('!block');
                            }
                        }

                    });
                    

                    // validate each section on initial load
                    
                    validateFields(parentStep, true);
                    
                });
            }

            // step prev
            const stepPrevs = document.querySelectorAll('[data-stepprev]');

            if ( stepPrevs.length ) {
                stepPrevs.forEach(function(stepPrev) {
                
                    var parentStep = stepPrev.closest('.step');
                    
                    stepPrev.addEventListener('click', function() {

                            
                        if ( parentStep.querySelector('[data-stepedit]') ) {
                            parentStep.querySelector('[data-stepedit]').classList.remove('!block');
                        }

                        var el = jQuery(parentStep);
                        var prev = el.prev('.step');

                        prev.nextAll('.step').find('.u-h2').addClass('opacity-50');

                        if ( prev ) {
                            el.find('.step-body').slideUp();

                            prev.find('.step-body').slideDown();
                        }
                            

                    });

                    // validate each section on initial load
                    
                    validateFields(parentStep, true);
                    
                });
            }

            if ( stepSubmits.length ) {
                stepSubmits.forEach(function(stepSubmit) {
                
                    var parentStep = stepSubmit.closest('.step');
                    
                    stepSubmit.addEventListener('click', function() {

                        if ( validateFields(parentStep) ) {
                            
                            bbpress.sendSteps();
                            
                        }

                    });

                    // validate each section on initial load
                    
                    validateFields(parentStep, true);
                    
                });
            }


            function validateFields(step, initial = false) {

                // clear steps message
                bbpress.clearStepsMsg();

                var hasError = false;

                if ( initial == false ) {
                    // check all required fields
                    var requireds = step.querySelectorAll('[required]');
                    if ( requireds.length ) {

                        requireds.forEach(function(required) {
                            if ( required.value == '' ) {
                                hasError = true;

                                if ( !required.classList.contains('error') ) {
                                    // add error message
                                    var errorMsg = required.dataset.errormsg;
                                    let error = document.createElement("div");
                                    error.classList.add('field-error-msg');
                                    error.innerHTML = errorMsg;
                                    required.after(error)
                                }

                                required.classList.add('error');
                                
                            } else {

                                required.classList.remove('error');
                                
                                if ( required.parentElement.querySelector('.field-error-msg') ) {
                                    required.parentElement.querySelector('.field-error-msg').remove();
                                }
                            }
                    
                        });
                        
                    }

                    // reg check
                    var regChecks = step.querySelectorAll('.reg-check');
                    if ( regChecks.length ) {

                        regChecks.forEach(function(regCheck) {

                            var required = regCheck.querySelector('input');

                            if ( required ) {
                                if ( required.value == '' ) {
                                    hasError = true;

                                    var errorMsg = 'Enter Reg';
                                    
                                    
                    
                                    if ( !regCheck.classList.contains('error') ) {
                                        // add error message
                                        let error = document.createElement("div");
                                        error.classList.add('field-error-msg');
                                        error.innerHTML = errorMsg;
                                        regCheck.after(error)
                                    }
                    
                                    regCheck.classList.add('error');
                                    
                                } else {
                                    regCheck.classList.remove('error');
                                    
                                    if ( regCheck.parentElement.querySelector('.field-error-msg') ) {
                                        regCheck.parentElement.querySelector('.field-error-msg').remove();
                                    }
                                }
                            }
                    
                        });
                        
                    }
                }
                
                if ( hasError ) {
                    
                    if ( step.querySelector('[data-stepnext]') ) {
                        step.querySelector('[data-stepnext]').disabled = true;
                    }

                    return false;

                } else {
                    if ( step.querySelector('[data-stepnext]') ) {
                        step.querySelector('[data-stepnext]').disabled = false;
                    }

                    return true;
                }
                
            }

            // on field change
            function fieldChanges() {
                var stepFields = document.querySelectorAll('.step input,.step textarea, .reg-check input');

                if ( stepFields ) {
                    stepFields.forEach(function(stepField) {
                        var step = stepField.closest('.step');
                        
                        stepField.addEventListener('keyup', function() {
                            validateFields(step);
                        });

                    });
                }
            }

            fieldChanges();

            function editStep() {

                steps.forEach(function(step) {

                    if ( step.querySelector('[data-stepedit]') ) {
                        step.querySelector('[data-stepedit]').addEventListener('click', function() {
                            

                                jQuery('.step .step-body').slideUp();

                                var el = jQuery(step);

                                el.find('.step-body').slideDown();
                                el.find('.u-h2').removeClass('opacity-50');
                                el.nextAll('.step').find('.u-h2').addClass('opacity-50');

                                step.querySelector('[data-stepedit]').classList.remove('!block');
                                el.nextAll('.step').find('[data-stepedit]').removeClass('!block');
                            
                        });
                    }
                });
            }

            editStep();

        }

    },

    sendSteps: function() {

        bbpress.disableSubmit();

        function updateTopics(post_id) {
            var stepsForm = jQuery('#steps-form');
            var bbbody = jQuery('.bbp-body');
            
            jQuery.ajax({
             type : "post",
             dataType : "json",
             url : myObj.ajaxUrl,
             data : {
                action:   'get_updated_topic',
                post_id: post_id
             },
             success: function(response) {
                            
                stepsForm.before(response.html_header);
                stepsForm.before(response.html);
                stepsForm.before(response.html_footer);
                stepsForm.remove();
                bbbody.prepend(response.html);
                 
                
             }
            })
        }
        
        var form = jQuery('form').serialize();
        
        jQuery.ajax({
            type : "post",
            dataType : "json",
            url : myObj.ajaxUrl,
            data : form,
            success: function(response) {
            
                if ( response.status == 401 ) {
                    bbpress.stepsMsg(response.message, 'error');

                    bbpress.enableSubmit();
                }

                if ( response.status == 200 ) {
                    updateTopics(response.new_post_ID);
                }
            
            }
        });


    },

    stepsMsg: function(message, type = 'error') {
        var msgWrap = jQuery('#steps-msg');

        if ( type == 'error' ) {
            msgWrap.html('<div class="steps-error bg-[red]/20 border-2 border-[red] p-3 rounded-sm shadow-sm font-bold">'+message+'</div>');
        }

        jQuery('#steps').animate({scrollTop: 0});
    },

    clearStepsMsg: function(message) {
        var msgWrap = jQuery('#steps-msg');
        msgWrap.html('');
    },

    closeModal: function(target) {

        var modal = document.querySelector('[data-bbmodal="'+target+'"]');
        if ( modal ) {
            modal.classList.remove('!visible', '!opacity-100');
        }

    },

    openModal: function(target) {

        var modal = document.querySelector('[data-bbmodal="'+target+'"]');
        if ( modal ) {
            modal.classList.add('!visible', '!opacity-100');
        }

    },

    triggerModal: function() {
        var modalOpen = document.querySelectorAll('[data-bbmodalt]');

        if ( modalOpen.length ) {
            modalOpen.forEach(function(modalTrigger) {
                modalTrigger.addEventListener('click', function() {
                    var target = modalTrigger.dataset.bbmodalt;
                    bbpress.openModal(target);
                });
            });
        }

        var modalClose = document.querySelectorAll('[data-bbmodalc]');

        if ( modalClose.length ) {
            modalClose.forEach(function(modalTrigger) {
                modalTrigger.addEventListener('click', function() {
                    var target = modalTrigger.dataset.bbmodalc;
                    bbpress.closeModal(target);
                });
            });
        }
    },

    enableSubmit: function(e) {
        const stepSubmits = document.querySelectorAll('[data-stepsubmit]');


        if ( stepSubmits.length ) {
            stepSubmits.forEach(function(stepSubmit) {
            
                stepSubmit.removeAttribute('disabled');
                stepSubmit.classList.remove('spinner');
            });
        }
    },

    disableSubmit: function() {
        const stepSubmits = document.querySelectorAll('[data-stepsubmit]');


        if ( stepSubmits.length ) {
            stepSubmits.forEach(function(stepSubmit) {
                stepSubmit.setAttribute('disabled', '');
                stepSubmit.classList.add('spinner');
                
            });
        }
    },

    topicResolved: function() {
        const resolveBtns = document.querySelectorAll('[data-marksolved]');

        if ( resolveBtns.length ) {

            resolveBtns.forEach(function(resolveBtn) {

                resolveBtn.addEventListener('click', function() {

                    resolveBtn.classList.add('spinner');
                    resolveBtn.setAttribute('disabled', true);

                    jQuery.ajax({
                        type : "post",
                        dataType : "json",
                        url : myObj.ajaxUrl,
                        data : {
                            action:   'mark_topic_solved',
                            topicid:   resolveBtn.dataset.topicid,
                            replyid:   resolveBtn.dataset.replyid,
                            marksolved:   resolveBtn.dataset.marksolved
                        },
                        success: function(response) {
                                        
                            if ( response.status == 200 ) {
                                window.location = location.protocol+'//'+location.host+location.pathname;
                            } else if (response.status == 500) {
                                alert('There was a problem');
                                console.log(response)
                            }
                            
                            
                        }
                    });
                });

            });
        }
    }

}

function bbp_ajax_call_custom( action, object, type, nonce, update_selector ) {
    var $data = {
        action : action,
        id     : object,
        type   : type,
        nonce  : nonce
    };

    jQuery.post( 'http://localhost/wp22/gon/wp/forums/topic/car-leaking-2-dont-delete/?bbp-ajax=true', $data, function ( response ) {
        if ( response.success ) {
            jQuery( update_selector ).html( response.content );
            alert(response.content)
        } else {
            if ( !response.content ) {
                response.content = bbpEngagementJS.generic_ajax_error;
            }
            window.alert( response.content );
        }
    } );
}

jQuery( '.favorite-toggles' ).each(function() {

    jQuery(this).on( 'click', 'span a.favorite-toggle', function( e ) {

        e.preventDefault();
		bbp_ajax_call_custom(
			'favorite',
			jQuery( this ).data( 'bbp-object-id'   ),
			jQuery( this ).data( 'bbp-object-type' ),
			jQuery( this ).data( 'bbp-nonce'       ),
			jQuery(this)
		);
	} );
});

window.addEventListener("DOMContentLoaded", function() {
    bbpress.stepsVal();
    bbpress.topicResolved();

    bbpress.triggerModal();
    
});


// input tags
(function(){

    "use strict"

    
    // Plugin Constructor
    var TagsInput = function(opts){
        this.options = Object.assign(TagsInput.defaults , opts);
        this.init();
    }

    // Initialize the plugin
    TagsInput.prototype.init = function(opts){
        this.options = opts ? Object.assign(this.options, opts) : this.options;

        if(this.initialized)
            this.destroy();
            
        if(!(this.orignal_input = document.getElementById(this.options.selector)) ){
            console.error("tags-input couldn't find an element with the specified ID");
            return this;
        }

        this.arr = [];
        this.wrapper = document.createElement('div');
        this.input = document.createElement('input');
        init(this);
        initEvents(this);

        this.initialized =  true;
        return this;
    }

    // Add Tags
    TagsInput.prototype.addTag = function(string){

        if(this.anyErrors(string))
            return ;

        this.arr.push(string);
        var tagInput = this;

        var tag = document.createElement('span');
        tag.className = this.options.tagClass;
        tag.innerText = string;

        var closeIcon = document.createElement('a');
        closeIcon.innerHTML = '&times;';
        
        // delete the tag when icon is clicked
        closeIcon.addEventListener('click' , function(e){
            e.preventDefault();
            var tag = this.parentNode;

            for(var i =0 ;i < tagInput.wrapper.childNodes.length ; i++){
                if(tagInput.wrapper.childNodes[i] == tag)
                    tagInput.deleteTag(tag , i);
            }
        })


        tag.appendChild(closeIcon);
        this.wrapper.insertBefore(tag , this.input);
        this.orignal_input.value = this.arr.join(',');

        return this;
    }

    // Delete Tags
    TagsInput.prototype.deleteTag = function(tag , i){
        tag.remove();
        this.arr.splice( i , 1);
        this.orignal_input.value =  this.arr.join(',');
        return this;
    }

    // Make sure input string have no error with the plugin
    TagsInput.prototype.anyErrors = function(string){
        if( this.options.max != null && this.arr.length >= this.options.max ){
            console.log('max tags limit reached');
            return true;
        }
        
        if(!this.options.duplicate && this.arr.indexOf(string) != -1 ){
            console.log('duplicate found " '+string+' " ')
            return true;
        }

        return false;
    }

    // Add tags programmatically 
    TagsInput.prototype.addData = function(array){
        var plugin = this;
        
        array.forEach(function(string){
            plugin.addTag(string);
        })
        return this;
    }

    // Get the Input String
    TagsInput.prototype.getInputString = function(){
        return this.arr.join(',');
    }


    // destroy the plugin
    TagsInput.prototype.destroy = function(){
        this.orignal_input.removeAttribute('hidden');

        delete this.orignal_input;
        var self = this;
        
        Object.keys(this).forEach(function(key){
            if(self[key] instanceof HTMLElement)
                self[key].remove();
            
            if(key != 'options')
                delete self[key];
        });

        this.initialized = false;
    }

    // Private function to initialize the tag input plugin
    function init(tags){
        tags.wrapper.append(tags.input);
        tags.wrapper.classList.add(tags.options.wrapperClass);
        tags.orignal_input.setAttribute('hidden' , 'true');
        tags.orignal_input.parentNode.insertBefore(tags.wrapper , tags.orignal_input);
    }

    // initialize the Events
    function initEvents(tags){
        tags.wrapper.addEventListener('click' ,function(){
            tags.input.focus();           
        });
        

        tags.input.addEventListener('keydown' , function(e){
            var str = tags.input.value.trim(); 

            if( !!(~[9 , 13 , 188].indexOf( e.keyCode ))  )
            {
                e.preventDefault();
                tags.input.value = "";
                if(str != "")
                    tags.addTag(str);
            }

        });
    }


    // Set All the Default Values
    TagsInput.defaults = {
        selector : '',
        wrapperClass : 'tags-input-wrapper',
        tagClass : 'tag',
        max : null,
        duplicate: false
    }

    window.TagsInput = TagsInput;

})();

var inputTags = document.querySelectorAll('.input-tags');

if ( inputTags.length ) {

    inputTags.forEach(function(inputTag) {

        // get current tags
        var currentTags = inputTag.dataset.tags;

        var tagInput1 = new TagsInput({
            selector: inputTag.id,
            duplicate : false,
            max : 10
        });

        if ( currentTags ) {
            tagInput1.addData(currentTags.split(","));
        }
    });
}



