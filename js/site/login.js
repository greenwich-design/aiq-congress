jQuery( document ).ready( function () {
			jQuery( '#loginform label[for="user_login"]' ).attr( 'id', 'user_label' );
			jQuery( '#loginform label[for="user_pass"]' ).attr( 'id', 'pass_label' );
			jQuery( '#registerform label[for="user_login"]' ).attr( 'id', 'user_label_register' );
			jQuery( '#registerform label[for="user_email"]' ).attr( 'id', 'email_label_register' );
			jQuery( '#lostpasswordform label[for="user_login"]' ).attr( 'id', 'user_label_lost' );

			var $label_user_login = jQuery( 'label#user_label' );
			$label_user_login.html( $label_user_login.find( 'input' ) );

			var $label_user_pass = jQuery( 'label#pass_label' );
			$label_user_pass.html( $label_user_pass.find( 'input' ) );

			var $label_user_register = jQuery( 'label#user_label_register' );
			$label_user_register.html( $label_user_register.find( 'input' ) );

			var $label_email_register = jQuery( 'label#email_label_register' );
			$label_email_register.html( $label_email_register.find( 'input' ) );

			var $label_user_lost = jQuery( 'label#user_label_lost' );
			$label_user_lost.html( $label_user_lost.find( 'input' ) );

			var loginform_user_login = 'Email Address';
			var loginform_user_pass = 'Password';

			jQuery( '#loginform #user_login' ).attr( 'placeholder', jQuery( '<div/>' ).html( loginform_user_login ).text() );
			jQuery( '#loginform #user_pass' ).attr( 'placeholder', jQuery( '<div/>' ).html( loginform_user_pass ).text() );

			var registerform_user_login = 'Username';
			var registerform_user_email = 'Email';

			jQuery( '#registerform #user_login' ).attr( 'placeholder', jQuery( '<div/>' ).html( registerform_user_login ).text() );
			jQuery( '#registerform #user_email' ).attr( 'placeholder', jQuery( '<div/>' ).html( registerform_user_email ).text() );

            var registerform_user_login = 'Username/Email';
            jQuery( '#lostpasswordform #user_login' ).attr( 'placeholder', jQuery( '<div/>' ).html( registerform_user_login ).text() );

			var lostpasswordform_user_login = 'Email Address';
			var resetpassform_pass1 = 'Add new password';
			var resetpassform_pass2 = 'Retype new password';

});