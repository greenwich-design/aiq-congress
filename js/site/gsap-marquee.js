
function initMarquees() {
    const marquees = document.querySelectorAll('.marquee:not(.done)');

    if ( marquees.length ) {

        marquees.forEach(function(marquee) {

            const inner = marquee.querySelector('.marquee__inner');
            const parts = marquee.querySelectorAll('.marquee__part');

            const duration = (parts.length * 5);

            let currentScroll = 0;
            let isScrollingDown = true;

            let tween = gsap.to(inner, {xPercent: -100, repeat: -1, duration: duration, ease: "linear"}).totalProgress(0.5);

            //gsap.set(inner, {xPercent: -50});

            /*
            window.addEventListener("scroll", function(){
            
                if ( window.pageYOffset > currentScroll ) {
                    isScrollingDown = true;
                } else {
                    isScrollingDown = false;
                }
                
                gsap.to(tween, {
                    timeScale: isScrollingDown ? 1 : -1
                });
                
                currentScroll = window.pageYOffset
            });*/

            marquee.classList.add('done');
        });
    }
}

initMarquees();