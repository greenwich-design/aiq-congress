var regForm;

jQuery(document).ready(function ($) {
    
	// Perform AJAX login/register on form submit
	$('form#login, form#register, form#lostpasswordform').on('submit', function (e) {
        
        if (!$(this).valid()) return false;
        
        $('p.c-login__status').addClass('active');
        $('p.c-login__status').show().text(ajax_auth_object.loadingmessage);
		action = 'ajaxlogin';
		username = 	$('form#login #username').val();
		password = $('form#login #password').val();
		email = '';
        company = '';
		security = $('form#login #security').val();
        phone = '';
		if ($(this).attr('id') == 'register') {
			action = 'ajaxregister';
            username = $('#signonname').val();
            username = $('#signonname').val();
            company = $('#company').val();
            phone = $('#phone').val();
			password = '';
        	email = $('#email').val();
        	security = $('#signonsecurity').val();
		}  

        if ($(this).attr('id') == 'lostpasswordform') {
			action = 'ajaxretrieve_password';
            username = $('form#lostpasswordform #username').val();
        	security = $('#lostpasswordform #security').val();
		}
		ctrl = $(this);


		$.ajax({
            type: 'POST',
            dataType: 'json',
            url: ajax_auth_object.ajaxurl,
            data: {
                'action': action,
                'username': username,
                'password': password,
				'email': email,
                'security': security,
                'company': company,
                'phone': phone
            },
            success: function (data) {
				ctrl.find('p.c-login__status').html(data.message);
                
				if (data.loggedin == true) {
                    setTimeout(function() {
                        document.location.href = ajax_auth_object.redirecturl;
                    }, 500);
                }

                if (data.loggedin == false && data.toapprove == false ) {
                    // if not logged in, there is an error.  skip panel back
                    var panelsWrap = document.querySelector('.c-form__panels');
                    if ( panelsWrap ) {
                        var panels = document.querySelectorAll('.c-form__panel');
                        var wrap = document.querySelector('.c-login-form__wrap');

                        if ( panels.length ) {
                            panels.forEach(function(panel) {
                                panel.classList.remove('c-form__panel--left');
                            });

                            wrap.classList.remove('showTerms');
                            panelsWrap.dataset.ind = 0;
                        }
                    }
                }
                
            }
        });
        e.preventDefault();
    });
	
	// Client side form validation
   if (jQuery("#register").length) {

        if ( jQuery("#register").hasClass('theme1') ) {

            jQuery.validator.addMethod("regPageRequired", function(value, element) {
                
                if ( element.classList.contains('regPageRequired') ) {
                    var parent = element.closest('.c-form__panels');
                    if ( parent.dataset.ind == 0 ) {
                        return !this.optional(element);
                    }
                }		
                
                return "dependency-mismatch";
            }, jQuery.validator.messages.required);

            jQuery.validator.addMethod("regagree", function(value, element) {
                
                if ( element.classList.contains('regagree') ) {
                    var parent = element.closest('.c-form__panels');
                    if ( parent.dataset.ind == 1 ) {
                        return !this.optional(element);
                    }
                }		
                
                return "dependency-mismatch";
            }, jQuery.validator.messages.required);

            regForm = jQuery("#register").validate();

            var nextPanelBtn = document.querySelectorAll('[data-nextpanel]');
            if ( nextPanelBtn.length ) {
                nextPanelBtn.forEach(function(btn, i) {
                    btn.addEventListener('click', function() {
                        var panels = btn.closest('.c-form__panels');
                        var parent = btn.closest('.c-form__panel');
                        var wrap = btn.closest('.c-login-form__wrap');
                        var next = parent.nextElementSibling;

                        if ( next && regForm.form() ) {
                            parent.classList.add('c-form__panel--left');
                            next.classList.add('c-form__panel--left');
                            wrap.classList.add('showTerms');
                            panels.dataset.ind = 1;
                        }

                        // fill out disclaimer dynamic details
                        var name = jQuery('#register #signonname');
                        var company = jQuery('#register #company');
                        var dynName = jQuery('.dyn-name');
                        var dynCompany = jQuery('.dyn-company');

                        if ( name && company ) {
                            dynName.text(name.val());
                            dynCompany.text(company.val());
                            
                        }
                    });
                });
            }

        } else {
            jQuery("#register").validate(
            { 
                rules: {
                        regagree: "required", // <- add this
                        
                    },
                    messages: {
                        regagree: "* You must agree to the terms", // <- add this
                        
                    }
            }
            );
        }


   } else if (jQuery("#login").length) {
		jQuery("#login").validate();
   }
});


function checkScroll(e) {
    var isFullyScrolled = (this.scrollTop + this.clientHeight + 5) >= this.scrollHeight;
    if ( isFullyScrolled ) {
        var target = document.querySelector('.c-form__check--validate');
        if ( target ) {
            target.classList.remove('c-form__check--validate');
        }
    }
}
    
document.addEventListener('DOMContentLoaded', function() {
    if ( document.querySelector('.c-form__note') ) {
        document.querySelector('.c-form__note').addEventListener('scroll', checkScroll);
        document.querySelector('.c-form__note').dispatchEvent(new CustomEvent('scroll'));
    }
});