var footerCtaTL;
const gsapAnimations = {

    
    general: () => {
 
    },

    toggleFooterSignup: () => {
        const footerSignup = document.querySelector('#footer-signup');

        if ( footerSignup.classList.contains('active') ) {
            gsap.to('#footer-signup', {height: 0});
            footerSignup.classList.remove('active');
        } else {
            gsap.to('#footer-signup', {height: 'auto'});
            footerSignup.classList.add('active');
        }
    },

    accordions: () => {
        const accordions = document.querySelectorAll('.accordion');

        accordions.forEach(function(accordion) {
            const accordion_titles = accordion.querySelectorAll('.accordion__title');

            accordion_titles.forEach(function(accordion_title) {
                accordion_title.addEventListener('click', function(e) {
                    const parentAccordion = accordion_title.closest('.accordion');
                    const parent = accordion_title.closest('.accordion__row');
                    const body = parent.querySelector('.accordion__body');
                    const accordion_toggle = parent.querySelector('.accordion__toggle');

                    
                    
                    const rows = Array.from(parentAccordion.querySelectorAll('.accordion__row')).filter(item => item !== parent);
                    rows.forEach(function(rowother) {
                        rowother.classList.remove('active');
                    });

                    const bodys = Array.from(parentAccordion.querySelectorAll('.accordion__body')).filter(item => item !== body);
                    bodys.forEach(function(bodyother) {
                        gsap.to(bodyother, {'height': '0', duration: 0.5});
                        setTimeout(function() {
                            bodyother.classList.remove('before:!h-0')
                        });
                    });

                    const accordion__titles_filtered = Array.from(accordion_titles).filter(item => item !== accordion_toggle);
                    accordion__titles_filtered.forEach(function(accordion_title_other) {
                        accordion_title_other.classList.remove('before:translate-x-0', 'text-white');
                    });

                    const accordion_toggles = Array.from(parentAccordion.querySelectorAll('.accordion__toggle')).filter(item => item !== accordion_toggle);
                    accordion_toggles.forEach(function(accordion_toggle_other) {
                        gsap.to(accordion_toggle_other, {rotation: 0, duration: 0.5});
                    });

                    parent.classList.toggle('active');

                    if ( parent.classList.contains('active') ) {
                        gsap.to(body, {'height': 'auto', duration: 0.5});
                        gsap.to(accordion_toggle, {rotation: 45, duration: 0.5});
                        accordion_title.classList.add('before:translate-x-0', 'text-white');
                        body.classList.add('before:!h-0');
                    } else {
                        gsap.to(body, {'height': '0', duration: 0.5});
                        gsap.to(accordion_toggle, {rotation: 0, duration: 0.5});
                        accordion_title.classList.remove('before:translate-x-0', 'text-white');
                        setTimeout(function() {
                            body.classList.remove('before:!h-0')
                        }, 500);
                    }
                    
                    
                });
            });
            
        });
    },

    parallax: () => {
        const parallaxs = document.querySelectorAll('.parallax');
        parallaxs.forEach(function(parallax) {
            gsap.to(parallax, {
                scrollTrigger: {
                    start: "top top",
                    end: "bottom top",
                    scrub: true
                },
                ease: Power0.easeNone,
                y: "100%"
            });
        });
    },

    init: () => {
        gsapAnimations.general();
        gsapAnimations.accordions();
        gsapAnimations.parallax();
    }

}

gsapAnimations.init();