var dropzone_options = {
    url                 		: myObj.ajaxUrl,
    timeout             		: 3 * 60 * 60 * 1000,
    acceptedFiles 				: 'image/*,application/pdf',
    addRemoveLinks				: true,
    maxFiles:					3,
    disablePreviews:            false
}

var dropzones = document.querySelectorAll('.media-dropzone');

var dropzoneMedia = [];

if ( dropzones.length ) {
    dropzones.forEach(function(dropzone, i) {
        dropzone_obj = new Dropzone( dropzone, dropzone_options );
        
        dropzone_obj.on('addedfile', function(file) {

            // change image thumb depending on file type
            var ext = file.name.split('.').pop();
            setTimeout(function() {
                var root = dropzone.closest('.dropzone-wrap').dataset.root;
                if (ext == "pdf") {
                    jQuery(file.previewElement).find(".dz-image img").attr("src", root+"/assets/img/pdf.svg");
                } else if (ext.indexOf("doc") != -1) {
                    jQuery(file.previewElement).find(".dz-image img").attr("src", "/Content/Images/word.png");
                } else if (ext.indexOf("xls") != -1) {
                    jQuery(file.previewElement).find(".dz-image img").attr("src", "/Content/Images/excel.png");
                }
            }, 100);
        });

        var currentFiles = dropzone.closest('.dropzone-wrap').querySelectorAll('.media-dropzone-inputs input');
        if ( currentFiles.length ) {
            currentFiles.forEach(function(currentFile) {
                var name = currentFile.value.split(/(\\|\/)/g).pop();
                var size = currentFile.dataset.filesize;
                let mockFile = { size: size, name: name, dataURL: currentFile.value };
                dropzone_obj.displayExistingFile(mockFile, currentFile.value, '', '', false);
                
            });
            
        }

        dropzoneMedia[dropzone.dataset.dropzone] = [];

        

        dropzone_obj.on(
        'sending',
        function(file, xhr, formData) {
            formData.append( 'action', 'file_upload' );
            formData.append( 'security', myObj.security );
        });

        dropzone_obj.on(
        'success',
        function(file, response) {
            
            var responseParsed = JSON.parse(response);

            var uuid = file.upload.uuid;
            var fileUrl = responseParsed.url;

            responseParsed.uuid = uuid;

            var files = dropzone.closest('.dropzone-wrap').querySelector('.media-dropzone-inputs');
            var fieldname = files.dataset.fieldname;

            var field = document.createElement('input');
            field.name = fieldname;
            field.value = responseParsed.url;
            field.dataset.uuid = uuid;
            field.type = 'hidden';

            files.appendChild(field);
            
            

            dropzoneMedia[dropzone.dataset.dropzone].push(responseParsed);
        });

        dropzone_obj.on(
        'removedfile',
        function(file) {
            
            console.log(file);
            var files = dropzone.closest('.dropzone-wrap').querySelector('.media-dropzone-inputs');
                
            if ( files.querySelector('input[value="'+file.dataURL+'"]') ) {
                console.log(files.querySelector('input[value="'+file.dataURL+'"]'));
                files.querySelector('input[value="'+file.dataURL+'"]').remove();
            } else {

                var uuid = file.upload.uuid;

                var inputs = document.querySelectorAll('input[data-uuid="'+uuid+'"]');

                if ( inputs ) {
                    inputs.forEach(function(input) {
                        input.remove();
                    });
                }

                for (let index = 0; index < dropzoneMedia[dropzone.dataset.dropzone].length; index++) {
                    
                    if ( dropzoneMedia[dropzone.dataset.dropzone][index].uuid == uuid ) {
                        dropzoneMedia[dropzone.dataset.dropzone].splice(0, (index+1));
                    }
                    
                }

            }
            
            
        });
    });
}