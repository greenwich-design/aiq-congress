
var App;

App = {

    fakeSelects: function() {
        const fakeSelects = document.querySelectorAll('.fake-select');
        fakeSelects.forEach(function(fakeSelect) {
            fakeSelect.querySelector('select').addEventListener('change', function() {
                const value = fakeSelect.querySelector('select').value;
                fakeSelect.querySelector(':scope > span.fake-select-value').innerHTML = value;
            });
        });
    },

    modals: function() {
      var modalTriggers = document.querySelectorAll('[data-modaltrigger]');

      if ( modalTriggers.length ) {
          modalTriggers.forEach(function(modalTrigger) {
              var id = modalTrigger.dataset.modaltrigger;
              var modal = document.querySelector('[data-modal="'+id+'"]');
              
              if ( modal ) {

                  modalTrigger.addEventListener('click', function(e) {

                      e.preventDefault();
                      
                      if ( !modal.classList.contains('!visible') ) {
                          modal.classList.add('!visible', '!opacity-100');
                      } else {
                          modal.classList.remove('!visible', '!opacity-100');
                      }
                  });
                  
              }
          });
      }
    },

    vhhack: function() {
        /**
         * 100VH hack
         */
        // First we get the viewport height and we multiple it by 1% to get a value for a vh unit
        var vh = window.innerHeight * 0.01;
        // Then we set the value in the --vh custom property to the root of the document
        document.documentElement.style.setProperty('--vh', vh+'px');
    },


    bindUI: function() {
        
        
    },

    init: function() {
        
        App.fakeSelects();
        App.modals();
        
    }
}

document.addEventListener("DOMContentLoaded", function() {
    
    App.bindUI();
    App.init();
});

window.addEventListener('resize', function() {
    
});




function historyback(url) {
    if (window.history.length > 2) {
        // if history is not empty, go back:
        window.history.back();
    } else if (url) {
        // go to specified fallback url:
        window.location = url;
    }
    
    return false;
}


function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays*24*60*60*1000));
  var expires = "expires="+ d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}
