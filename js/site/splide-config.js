



function initSplides() {
    const splides = document.querySelectorAll('.splide');

    if ( splides.length ) {

        splides.forEach(function(splide) {
            
            var splideItem = new Splide( splide );

            splideItem.mount();

            var withSelector = splide.closest('.splide-with-selector');
            if ( withSelector ) {
                const selectors = withSelector.querySelectorAll('.splide__selector li');
                if ( selectors.length ) {

                    selectors.forEach(function(selector, i) {


                        selector.querySelector('button').addEventListener('click', function() {

                            selectors.forEach(function(selector2, i) {
                                selector2.classList.remove('before:bg-black', 'text-white');
                            });

                            selector.classList.add('before:bg-black', 'text-white');

                            splideItem.go(i);
                            const { Autoplay } = splideItem.Components;
                            Autoplay.pause();
                        })
                    });
                }
            }

            // add lazyload to cloned splide slides
            lazyLoadImages();
        });
    }
}

document.addEventListener( 'DOMContentLoaded', function() {
    initSplides();
});