
function initOffcuts() {
    const offcutWraps = document.querySelectorAll('[data-offcutwrap]'); 

    if ( offcutWraps.length ) {
        offcutWraps.forEach(function(offcutWrap) {
            const offcutEls = offcutWrap.querySelectorAll('[data-offcutel]');
            const offcutTriggers = offcutWrap.querySelectorAll('[data-offcuttrigger]');

            if ( offcutTriggers.length ) {
                offcutTriggers.forEach(function(offcutTrigger, i) {
                    offcutTrigger.addEventListener('mouseover', function() {
                        offcutEls[i].classList.remove('opacity-0');
                    });

                    offcutTrigger.addEventListener('mouseout', function() {
                        offcutEls[i].classList.add('opacity-0');
                    });
                });
            }
        });
    }
}

initOffcuts();