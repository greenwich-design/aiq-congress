async function fetchElement(url, selector) {
    const data = await fetch(url).then(res => res.text());
    const parsed = new DOMParser().parseFromString(data, 'text/html');
    return parsed.querySelector(selector);
}

function ajaxLoad() {
    let $mainContent = document.querySelector('#main'),
        siteUrl = top.location.host.toString(),
        targets = document.querySelectorAll('a[href*="'+siteUrl+'"]:not([href*="/wp-admin/"]):not([href*="/wp-login.php"]):not([href$="/feed/"]):not(.noajax)'),
        url = '',
        clickedEl = '';
        
        
    function delegate(el, evt, sel, handler) {
        el.addEventListener(evt, function(event) {
            var t = event.target;
            
            while (t && t !== this) {
                if (t.matches(sel)) {
                    handler.call(t, event);
                    event.preventDefault();
                }
                t = t.parentNode;
            }

            
        });
    }

    delegate(document, "click", 'a[href*="'+siteUrl+'"]:not([href*="/wp-admin/"]):not([href*="/wp-login.php"]):not([href$="/feed/"]):not(.noajax)', function(event) {
        
        App.navClose();
        App.mainFadeOut();
        
        window.scrollTo(0, 0);
        
        let target = this;

        setTimeout(function() {
            handleClick(target);
        }, 500);
    });

    function handleClick(e) {

        window.history.pushState("object or string", "Title", e.pathname);
        var popStateEvent = new PopStateEvent('popstate', { state: '' });
        dispatchEvent(popStateEvent);

    }
    
    window.addEventListener('popstate', async function(){
        url = window.location.href;

        if (!url) {
            return;
        }

        
        const myElements = await fetchElement(url, "body");

        if ( myElements ) {

            // change page title element
            if ( myElements.dataset.pagetitle ) {
                document.querySelector('title').innerText = myElements.dataset.pagetitle;
            }

            document.querySelector("#main").innerHTML = "";
            var mainContent = myElements.querySelectorAll('#main > *');
            var newBodyClasses = myElements.className;

            var imgs = myElements.querySelectorAll('#main [loading="eager"]');

            // add new body class
            document.querySelector('body').className = newBodyClasses;

            document.querySelector("#main").append(...mainContent); 
            
            
            // load various ui functions
            
            // if there's images which are to be loaded eagerly, wait till last then fade in
            if ( imgs.length ) {
                imgs[imgs.length - 1].onload = function() {
                    App.mainFadeIn();
                }
            } else {
                App.mainFadeIn();
            }

            // safari fix, images with srcset don't always draw properly after ajax load
            if (navigator.userAgent.match(/Version\/[\d.]+.*Safari/)) {
                document.querySelectorAll('img').forEach((img) => {
                  img.outerHTML = img.outerHTML; // eslint-disable-line no-param-reassign, no-self-assign
                });
            }

            App.init();
            
            lazyLoadImages()
            lazyLoadVideos();

            initMarquees();
            initOffcuts();
            initSplides();

            gsapAnimations.accordions();
            gsapAnimations.parallax();

            // clutch widget
            const clutchWidget = document.querySelector('.clutch-widget');
            const clutchScript = document.querySelector('script[src="https://widget.clutch.co/static/js/widget.js"]');
            
            if ( clutchScript && window.CLUTCHCO ) {
                window.CLUTCHCO.Destroy();
                clutchScript.remove();
            }

            if ( clutchWidget ) {

                const script = document.createElement("script")
                script.type = "text/javascript"
                script.src = "https://widget.clutch.co/static/js/widget.js"
                script.async = true
                document.body.appendChild(script);

                script.onload = () => {
                    window.CLUTCHCO.Init()
                }
            }

            
        }
       
            
    });

}

document.addEventListener('DOMContentLoaded', () => {
    ajaxLoad();
});